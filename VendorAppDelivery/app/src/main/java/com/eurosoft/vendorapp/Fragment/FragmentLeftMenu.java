package com.eurosoft.vendorapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.ActivityLogin;
import com.eurosoft.vendorapp.ActivitySettings;
import com.eurosoft.vendorapp.ActivityStoreList;
import com.eurosoft.vendorapp.ActivtyStoreInfo;
import com.eurosoft.vendorapp.Adapter.AdapterMenu;
import com.eurosoft.vendorapp.Adapter.AdapterStoreList;
import com.eurosoft.vendorapp.Constants;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Service.OrderService;
import com.eurosoft.vendorapp.R;
import com.fxn.stash.Stash;

import org.jetbrains.annotations.NotNull;

public class FragmentLeftMenu extends Fragment implements View.OnClickListener{


    private RelativeLayout settingsRl,logoutRl,storeSettingsRl;
    private LinearLayout viewLL;
    private TextView name;
    private LoginResponse loginResponse;
    private GetAppSettings appSettings;
    private RecyclerView recyclerView;
    public static Fragment newInstance() {
        FragmentLeftMenu f = new FragmentLeftMenu();
        return f;
    }


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left_menu, container, false);
       /* Stash.init(getContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);*/
        init(view);


        return view;
    }

    private void init(View view) {

        settingsRl = view.findViewById(R.id.settingsRl);
        logoutRl = view.findViewById(R.id.logoutRl);
        storeSettingsRl = view.findViewById(R.id.storeSettingsRl);
        name=view.findViewById(R.id.name);
        loginResponse= (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        appSettings = (GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);
        viewLL= view.findViewById(R.id.viewLl);
        recyclerView=view.findViewById(R.id.recyclerView);
        logoutRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stash.clearAll();
                stopService();
                Intent mainIntent = new Intent(getActivity(), ActivityLogin.class);
                startActivity(mainIntent);
            }
        });
        name.setText(""+loginResponse.getUserName());
        settingsRl.setOnClickListener(this);
        storeSettingsRl.setOnClickListener(this);
      //addDynamicMenu();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){


            case R.id.settingsRl:
                startActivity(new Intent(getActivity(), ActivitySettings.class));
                break;

            case R.id.storeSettingsRl:
                startActivity(new Intent(getActivity(), ActivityStoreList.class));
                break;

                case R.id.logoutRl:
                Stash.clearAll();
                stopService();
                Toast.makeText(getActivity(),"ABC",Toast.LENGTH_SHORT).show();
                Intent mainIntent = new Intent(getActivity(), ActivityLogin.class);
                startActivity(mainIntent);
                break;

        }
    }
    private void stopService(){
        Intent stopIntent = new Intent(getActivity(), OrderService.class);
        stopIntent.setAction(Constants.STARTFOREGROUND_ACTION);
        getActivity().stopService(stopIntent);
    }
    private void addDynamicMenu(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
      AdapterMenu adapterMenu=new AdapterMenu(getActivity(), appSettings.getMenuList(), new AdapterMenu.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position,String key) {
                switch (view.getId()){
                    case R.id.menusRl:
                        if(key.equals("Stores")){
                            startActivity(new Intent(getActivity(), ActivityStoreList.class));
                        }
                        break;
                }

            }
        });
        recyclerView.setAdapter(adapterMenu);
        adapterMenu.notifyDataSetChanged();
    }
}
