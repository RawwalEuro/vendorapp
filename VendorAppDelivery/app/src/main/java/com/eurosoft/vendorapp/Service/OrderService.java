package com.eurosoft.vendorapp.Service;

import static com.eurosoft.vendorapp.Constants.CHANNEL_ID1;
import static com.eurosoft.vendorapp.Constants.EXTRA_STARTED_FROM_NOTIFICATION;
import static com.eurosoft.vendorapp.Constants.FireBaseOrderParameter;
import static com.eurosoft.vendorapp.Constants.OrderIDJOFFER;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.eurosoft.vendorapp.ActivityNewOrder;
import com.eurosoft.vendorapp.Constants;
import com.eurosoft.vendorapp.HomeActivty;
import com.eurosoft.vendorapp.Models.MasterPojo;
import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.R;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.eurosoft.vendorapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderService extends Service {
    private DatabaseReference mFirebaseRefrence;
    private FirebaseDatabase mFirebaseDatabase;
    private String orderObject = "";
    private AppStatus appStatus;
    private NotificationManager notificationManager;
    private LoginResponse vendorObj;
    private ApiInterface apiInterface;
    private MasterPojo masterPojo = new MasterPojo();


    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getAction().equals(Constants.STARTFOREGROUND_ACTION)) {
            Log.i("TAG", "Received Start Foreground Intent ");
        } else if (intent.getAction().equals(Constants.STOPFOREGROUND_ACTION)) {
            Log.i("TAG", "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelfResult(startId);
        }
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        appStatus = AppStatus.getInstance(this);
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        apiInterface = APIClient.getClient().create(ApiInterface.class);

        try {
            mFirebaseRefrence = mFirebaseDatabase.getReference().child("1").child(vendorObj.getId().toString());
            orderProcess(intent);
        } catch (Exception e) {
            Log.e("ExceptionTag", e.getMessage());
        }

        startForeground(1, getNotification());

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private Notification getNotification() {
        Intent intent = new Intent(OrderService.this, ActivityNewOrder.class);
        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);
        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(OrderService.this, HomeActivty.class), 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(OrderService.this)
                .setContentIntent(activityPendingIntent)
                //  .addAction(R.drawable.ic_cancel, getString(R.string.remove_location_updates),
                //  .servicePendingIntent
                .setContentText("Deli Dragon Vendor App is running")
                //   .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_notification)
                // .setTicker(text)
                .setWhen(System.currentTimeMillis());
        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID1);// Channel ID
        }
        return builder.build();
    }

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public OrderService getService() {
            // Return this instance of Service so clients can call public methods
            return OrderService.this;
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(CHANNEL_ID1, "Channel 1", NotificationManager.IMPORTANCE_HIGH);
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(serviceChannel);
        }
    }

    private void orderProcess(Intent finalIntent) {
        mFirebaseRefrence.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getKey().contains(vendorObj.getId().toString())) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        Log.e("ActivityFirebase", "SERVICES" + ds.getChildren().toString());
                        if (ds.child(FireBaseOrderParameter).exists()) {
                            readDataFromFireBase(ds, finalIntent);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ExceptionTag", "" + databaseError.getMessage());
            }
        });
    }

    public void readDataFromFireBase(DataSnapshot ds, Intent finalIntent) {
        Log.e("ActivityFirebase", "ReadDataFromFireBase" + ds.getChildren().toString());
        orderObject = ds.child(FireBaseOrderParameter).getValue(String.class);
        try {
            readRowFromFirebase(orderObject, finalIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readRowFromFirebase(String orderId, Intent intent) {
        try {
            if (appStatus.isOnline()) {
                if (OrderIDJOFFER == 0) {
                    int morderid= Integer.parseInt(orderId);
                    if (OrderIDJOFFER != morderid) {
                        OrderIDJOFFER = morderid;
                        getOrderData(vendorObj.getId() + "", orderId, intent);
                        Log.e("ActivityFirebase", "GetOrderData");
                    } else {
                        Log.e("ActivityFirebase", "OrderIDJOFFER" + OrderIDJOFFER);
                    }
                }
                else {
                    Log.e("ActivityFirebase", "OrderIDJOFFER is not zero: " + OrderIDJOFFER);
                }
            } else {
                Log.e("ActivityFirebase", "OrderIDJOFFER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getOrderData(String userid, String orderId, Intent finalIntent) {
        Call<WebResponse<OrderDataModel>> call = apiInterface.getOrderData(userid, orderId, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponse<OrderDataModel>>() {
            @Override
            public void onResponse(Call<WebResponse<OrderDataModel>> call, Response<WebResponse<OrderDataModel>> response) {
                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();
                    OrderDataModel orderDataModel = (OrderDataModel) webResponse.getData();
                    if (sucess) {
                        Log.e("LocationService", "SingleLaunchActivity");
                        singleLaunchActivity(finalIntent, orderDataModel);
                    }
                    else {
                        Toast.makeText(getApplicationContext(),""+message,Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.e("ExceptionTag", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<OrderDataModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    public void singleLaunchActivity(final Intent finalIntent, OrderDataModel orderDataModel) {
        finalIntent.setComponent(new ComponentName(getBaseContext(), ActivityNewOrder.class.getName()));
        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (orderDataModel != null) {
            finalIntent.putExtra("order_detail", (Parcelable) orderDataModel);
        }
        startActivity(finalIntent);
        //   Log.e("OrderID Arraylist", "" + orderDataModel.get(0).getPaymentStatusId());
    }
}
