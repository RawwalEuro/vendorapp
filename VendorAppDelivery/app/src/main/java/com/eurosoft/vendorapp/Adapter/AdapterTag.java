package com.eurosoft.vendorapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.Store;
import com.eurosoft.vendorapp.R;

import java.util.List;

public class AdapterTag extends RecyclerView.Adapter<AdapterTag.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private List<Store.StoreTagsList> listStore;
    private Context context;

    public AdapterTag(Context ctx,List<Store.StoreTagsList> list, OnItemClickListener onItemClickListener) {
        context = ctx;
        listStore = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public List<Store.StoreTagsList> getTagList(){
        return listStore;
    }
    public void setTagList( List<Store.StoreTagsList> list){
        listStore=list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_taglist, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Store.StoreTagsList storeTagsList = listStore.get(position);
        holder.tagName.setText("" + storeTagsList.getName());

        holder.tagName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v,position);
            }
        });


    }


    @Override
    public int getItemCount() {
        return listStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView tagName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tagName = itemView.findViewById(R.id.tag_name);


        }
    }
}
