package com.eurosoft.vendorapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.eurosoft.vendorapp.Service.OrderService;
import com.fxn.stash.Stash;

public class ActivitySplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        Stash.init(this);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

                Log.e("IS_LOGGED_IN",Stash.getBoolean(Constants.IS_LOGGED_IN) + "");
                if (Stash.getBoolean(Constants.IS_LOGGED_IN)) {
                    Intent intent = new Intent(ActivitySplash.this, HomeActivty.class);
                    Intent serviceIntent = new Intent(ActivitySplash.this, OrderService.class);
                    serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
                    startService(serviceIntent);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(ActivitySplash.this, ActivityLogin.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);


    }

}
