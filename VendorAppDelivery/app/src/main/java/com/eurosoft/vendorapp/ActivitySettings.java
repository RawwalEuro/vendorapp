package com.eurosoft.vendorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vendorapp.Models.MasterPojo;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.InputValidatorHelper;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySettings extends AppCompatActivity {

    RelativeLayout rlProgressBar,mainRl;
    private LoginResponse vendorObj;
    private ApiInterface apiService;
    private EditText userPhoneNo,userAddress;
    private TextView userName,userEmail;
    private MasterPojo masterPojo;
    private InputValidatorHelper inputValidatorHelper;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initView();
        findViewById(R.id.nav_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inputValidatorHelper.isNullOrEmpty(userName.getText().toString())) {
                    Toast.makeText(ActivitySettings.this, masterPojo.getUserNameIsEmpty(), Toast.LENGTH_SHORT).show();
                    return;

                }

                if (inputValidatorHelper.isNullOrEmpty(userEmail.getText().toString())) {
                    Toast.makeText(ActivitySettings.this, masterPojo.getNotAValidEmail(), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (inputValidatorHelper.isNullOrEmpty(userAddress.getText().toString())) {
                    Toast.makeText(ActivitySettings.this, "Address is Empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (inputValidatorHelper.isNullOrEmpty(userPhoneNo.getText().toString())) {
                    Toast.makeText(ActivitySettings.this, "PhoneNo is Empty", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (!inputValidatorHelper.isValidEmail(userEmail.getText().toString())) {
                    Toast.makeText(ActivitySettings.this, masterPojo.getUserNameIsEmpty(), Toast.LENGTH_SHORT).show();
                    return;

                }
                callApiForUpdatingProfile(vendorObj.getId(),userName.getText().toString(),userEmail.getText().toString(),vendorObj.getUserPassword(),vendorObj.getCulture(),userPhoneNo.getText().toString());
            }
        });
    }
    private void callApiForUpdatingProfile(int id,String username,String email, String password, String culture,String userphone){
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;

        }
        LoginResponse loginResponse=new LoginResponse(id,username,email,password,culture,userphone);
        Call<WebResponse<LoginResponse>> call = apiService.updateprofile(loginResponse,vendorObj.getToken()+"");
        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                         Stash.put(Constants.LOGIN_OBJ,response.body().getData());
                        progressLoader.progressVisiblityGone();
                        Toast.makeText(ActivitySettings.this, "" + message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ActivitySettings.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivitySettings.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivitySettings.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void initView(){
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        apiService  = APIClient.getClient().create(ApiInterface.class);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressLoader = new ProgressLoader(ActivitySettings.this,rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityGone();
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
        userPhoneNo = findViewById(R.id.userPhone);
        userAddress = findViewById(R.id.userAddress);
        inputValidatorHelper = new InputValidatorHelper();
        userPhoneNo.setHint(""+vendorObj.getUserPhone());
        userAddress.setHint("Empty");
        userName.setText(""+vendorObj.getUserName());
        userEmail.setText(""+vendorObj.getUserEmail());
        userPhoneNo.setText(""+vendorObj.getUserPhone());
        userAddress.setText("Empty");
        masterPojo=new MasterPojo();
    }



}