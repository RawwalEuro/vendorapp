package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetAppSettings implements Serializable {

    @SerializedName("WebsiteSettingData")
    @Expose
    private WebsiteSettingData websiteSettingData;
    @SerializedName("RegionSettingData")
    @Expose
    private RegionSettingData regionSettingData;
    @SerializedName("Languages")
    @Expose
    private List<Language> languages = null;
    @SerializedName("PaymentTypes")
    @Expose
    private List<PaymentType> paymentTypes = null;
    @SerializedName("BookingTypes")
    @Expose
    private List<BookingType> bookingTypes = null;
    @SerializedName("OrderStatus")
    @Expose
    private List<Orderstatus> orderStatus = null;
    @SerializedName("StoreTagList")
    @Expose
    private List<StoreTag> storeTagList = null;
    @SerializedName("MenuList")
    @Expose
    private List<Menu> menuList = null;

    public List<StoreType> getStoreTypeList() {
        return storeTypeList;
    }

    public void setStoreTypeList(List<StoreType> storeTypeList) {
        this.storeTypeList = storeTypeList;
    }

    @SerializedName("StoreTypeList")
    @Expose
    private List<StoreType> storeTypeList = null;


    public List<StoreTag> getStoreTagList() {
        return storeTagList;
    }

    public void setStoreTagList(List<StoreTag> storeTagList) {
        this.storeTagList = storeTagList;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public WebsiteSettingData getWebsiteSettingData() {
        return websiteSettingData;
    }

    public void setWebsiteSettingData(WebsiteSettingData websiteSettingData) {
        this.websiteSettingData = websiteSettingData;
    }

    public RegionSettingData getRegionSettingData() {
        return regionSettingData;
    }

    public void setRegionSettingData(RegionSettingData regionSettingData) {
        this.regionSettingData = regionSettingData;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<PaymentType> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentType> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public List<BookingType> getBookingTypes() {
        return bookingTypes;
    }

    public void setBookingTypes(List<BookingType> bookingTypes) {
        this.bookingTypes = bookingTypes;
    }

    public List<Orderstatus> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(List<Orderstatus> orderStatus) {
        this.orderStatus = orderStatus;
    }
    public class StoreTag {

        @SerializedName("Id")
        @Expose
        private Integer id;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public Integer getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(Integer sortOrder) {
            this.sortOrder = sortOrder;
        }

        public Boolean getDeleted() {
            return isDeleted;
        }

        public void setDeleted(Boolean deleted) {
            isDeleted = deleted;
        }

        public List<Object> getTblStoreAssignTags() {
            return tblStoreAssignTags;
        }

        public void setTblStoreAssignTags(List<Object> tblStoreAssignTags) {
            this.tblStoreAssignTags = tblStoreAssignTags;
        }

        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("BaseUrl")
        @Expose
        private String baseUrl;
        @SerializedName("Icon")
        @Expose
        private String icon;
        @SerializedName("SortOrder")
        @Expose
        private Integer sortOrder;
        @SerializedName("IsDeleted")
        @Expose
        private Boolean isDeleted;
        @SerializedName("tbl_StoreAssignTags")
        @Expose
        private List<Object> tblStoreAssignTags = null;
    }

    public class Menu {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("MenuName")
        @Expose
        private String menuName;
        @SerializedName("MenuUrl")
        @Expose
        private String menuUrl;
        @SerializedName("MenuIcon")
        @Expose
        private String menuIcon;
        @SerializedName("SubmenuId")
        @Expose
        private Integer submenuId;
        @SerializedName("SortOrder")
        @Expose
        private Integer sortOrder;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMenuName() {
            return menuName;
        }

        public void setMenuName(String menuName) {
            this.menuName = menuName;
        }

        public String getMenuUrl() {
            return menuUrl;
        }

        public void setMenuUrl(String menuUrl) {
            this.menuUrl = menuUrl;
        }

        public String getMenuIcon() {
            return menuIcon;
        }

        public void setMenuIcon(String menuIcon) {
            this.menuIcon = menuIcon;
        }

        public Integer getSubmenuId() {
            return submenuId;
        }

        public void setSubmenuId(Integer submenuId) {
            this.submenuId = submenuId;
        }

        public Integer getSortOrder() {
            return sortOrder;
        }

        public void setSortOrder(Integer sortOrder) {
            this.sortOrder = sortOrder;
        }
    }
    public class StoreType {
        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("StoreTypeName")
        @Expose
        private String storeTypeName;
        @SerializedName("isactive")
        @Expose
        private Boolean isactive;
        @SerializedName("createdate")
        @Expose
        private String createdate;
        @SerializedName("IsDeleted")
        @Expose
        private Boolean isDeleted;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getStoreTypeName() {
            return storeTypeName;
        }

        public void setStoreTypeName(String storeTypeName) {
            this.storeTypeName = storeTypeName;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }

        public String getCreatedate() {
            return createdate;
        }

        public void setCreatedate(String createdate) {
            this.createdate = createdate;
        }

        public Boolean getDeleted() {
            return isDeleted;
        }

        public void setDeleted(Boolean deleted) {
            isDeleted = deleted;
        }

        public List<Object> getTblStoreRequest() {
            return tblStoreRequest;
        }

        public void setTblStoreRequest(List<Object> tblStoreRequest) {
            this.tblStoreRequest = tblStoreRequest;
        }

        @SerializedName("tbl_StoreRequest")
        @Expose
        private List<Object> tblStoreRequest = null;
    }
}
