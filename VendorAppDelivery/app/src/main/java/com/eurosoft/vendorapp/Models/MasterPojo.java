package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MasterPojo {

    @SerializedName("userNameIsEmpty")
    private String userNameIsEmpty = "User name is empty";

    @SerializedName("passwordIsEmpty")
    private String passwordIsEmpty = "Password is empty";

    @SerializedName("checkInternet")
    private String checkInternet = "Please check your internet";

    @SerializedName("Email")
    private String email = "UserName";

    @SerializedName("Login")
    private String login = "Login";

    @SerializedName("Password")
    private String password = "Password";

    @SerializedName("notAValidEmail")
    private String notAValidEmail = "Not a valid email";

    @SerializedName("forgotPass")
    private String forgotPass = "Forgot Password?";

    @SerializedName("resetPassword")
    private String resetPassword = "Reset Password";


    public String getResetPassword() {
        return   resetPassword == null || resetPassword.isEmpty() ? "Reset Password" : forgotPass;
    }

    public String getForgotPass() {
        return  forgotPass == null || forgotPass.isEmpty() ? "Forget Password?" : forgotPass;
    }

    public String getNotAValidEmail() {
        return  notAValidEmail == null || notAValidEmail.isEmpty() ? "Not a valid email" : notAValidEmail;
    }

    public MasterPojo() {

    }

    public String getUserNameIsEmpty() {
        return userNameIsEmpty = userNameIsEmpty == null || userNameIsEmpty.isEmpty() ? "User name is empty" : userNameIsEmpty;
    }

    public String getPasswordIsEmpty() {
        return passwordIsEmpty = passwordIsEmpty == null || passwordIsEmpty.isEmpty() ? "Password is empty" : passwordIsEmpty;
    }

    public String getCheckInternet() {
        return checkInternet = checkInternet == null || checkInternet.isEmpty() ? "Check your internet" : checkInternet;
    }

    public String getEmail() {
        return email = email == null || email.isEmpty() ? "Email" : email;
    }

    public String getLogin() {
        return login = login == null || login.isEmpty() ? "Login" : login;
    }

    public String getPassword() {
        return password = password == null || password.isEmpty() ? "Password" : password;
    }
}
