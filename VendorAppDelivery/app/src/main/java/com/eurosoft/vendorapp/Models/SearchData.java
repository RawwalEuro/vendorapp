package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchData {

       @SerializedName("CustomerList")
        @Expose
        private List<Customer> customerList = null;
        @SerializedName("DriverList")
        @Expose
        private List<Driver> driverList;

        public List<Customer> getCustomerList() {
            return customerList;
        }

        public void setCustomerList(List<Customer> customerList) {
            this.customerList = customerList;
        }

        public List<Driver> getDriverList() {
            return driverList;
        }

        public void setDriverList(List<Driver> driverList) {
            this.driverList = driverList;
        }

    public class Driver {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("DriverNo")
        @Expose
        private String driverNo;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("VehicleType")
        @Expose
        private String vehicleType;
        @SerializedName("DriverLatitude")
        @Expose
        private Double driverLatitude;
        @SerializedName("DriverLongitude")
        @Expose
        private Double driverLongitude;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDriverNo() {
            return driverNo;
        }

        public void setDriverNo(String driverNo) {
            this.driverNo = driverNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(String vehicleType) {
            this.vehicleType = vehicleType;
        }

        public Double getDriverLatitude() {
            return driverLatitude;
        }

        public void setDriverLatitude(Double driverLatitude) {
            this.driverLatitude = driverLatitude;
        }

        public Double getDriverLongitude() {
            return driverLongitude;
        }

        public void setDriverLongitude(Double driverLongitude) {
            this.driverLongitude = driverLongitude;
        }
    }

public class Customer {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CustomerGroupdId")
    @Expose
    private Integer customerGroupdId;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerFirstName")
    @Expose
    private String customerFirstName;
    @SerializedName("CustomerLastName")
    @Expose
    private String customerLastName;
    @SerializedName("CustomerEmail")
    @Expose
    private String customerEmail;
    @SerializedName("CustomerPassword")
    @Expose
    private String customerPassword;
    @SerializedName("CustomerPhoto")
    @Expose
    private Object customerPhoto;
    @SerializedName("CustomerPhoneNumber")
    @Expose
    private String customerPhoneNumber;
    @SerializedName("CustomerAddress")
    @Expose
    private String customerAddress;
    @SerializedName("CustomerLocationTypeId")
    @Expose
    private Object customerLocationTypeId;
    @SerializedName("CustomerLocationType")
    @Expose
    private Object customerLocationType;
    @SerializedName("CustomerLatitude")
    @Expose
    private Object customerLatitude;
    @SerializedName("CustomerLongitude")
    @Expose
    private Object customerLongitude;
    @SerializedName("CustomerDeviceId")
    @Expose
    private Object customerDeviceId;
    @SerializedName("CustomerVerificationCode")
    @Expose
    private Object customerVerificationCode;
    @SerializedName("IsCustomerEmailVerify")
    @Expose
    private Boolean isCustomerEmailVerify;
    @SerializedName("IsCustomerNumberVerify")
    @Expose
    private Boolean isCustomerNumberVerify;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerGroupdId() {
        return customerGroupdId;
    }

    public void setCustomerGroupdId(Integer customerGroupdId) {
        this.customerGroupdId = customerGroupdId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public Object getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(Object customerPhoto) {
        this.customerPhoto = customerPhoto;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Object getCustomerLocationTypeId() {
        return customerLocationTypeId;
    }

    public void setCustomerLocationTypeId(Object customerLocationTypeId) {
        this.customerLocationTypeId = customerLocationTypeId;
    }

    public Object getCustomerLocationType() {
        return customerLocationType;
    }

    public void setCustomerLocationType(Object customerLocationType) {
        this.customerLocationType = customerLocationType;
    }

    public Object getCustomerLatitude() {
        return customerLatitude;
    }

    public void setCustomerLatitude(Object customerLatitude) {
        this.customerLatitude = customerLatitude;
    }

    public Object getCustomerLongitude() {
        return customerLongitude;
    }

    public void setCustomerLongitude(Object customerLongitude) {
        this.customerLongitude = customerLongitude;
    }

    public Object getCustomerDeviceId() {
        return customerDeviceId;
    }

    public void setCustomerDeviceId(Object customerDeviceId) {
        this.customerDeviceId = customerDeviceId;
    }

    public Object getCustomerVerificationCode() {
        return customerVerificationCode;
    }

    public void setCustomerVerificationCode(Object customerVerificationCode) {
        this.customerVerificationCode = customerVerificationCode;
    }

    public Boolean getIsCustomerEmailVerify() {
        return isCustomerEmailVerify;
    }

    public void setIsCustomerEmailVerify(Boolean isCustomerEmailVerify) {
        this.isCustomerEmailVerify = isCustomerEmailVerify;
    }

    public Boolean getIsCustomerNumberVerify() {
        return isCustomerNumberVerify;
    }

    public void setIsCustomerNumberVerify(Boolean isCustomerNumberVerify) {
        this.isCustomerNumberVerify = isCustomerNumberVerify;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}

}
