package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Store {

    @SerializedName("StoreData")
    @Expose
    private StoreData storeData;
    @SerializedName("StoreTagsData")
    @Expose
    private List<StoreTagsList> storeTagsData = null;
    @SerializedName("StoreTimingData")
    @Expose
    private List<StoreTimingList> storeTimingData = null;

    public StoreData getStoreData() {
        return storeData;
    }

    public void setStoreData(StoreData storeData) {
        this.storeData = storeData;
    }

    public List<StoreTagsList> getStoreTagsData() {
        return storeTagsData;
    }

    public void setStoreTagsData(List<StoreTagsList> storeTagsData) {
        this.storeTagsData = storeTagsData;
    }

    public List<StoreTimingList> getStoreTimingData() {

        if(storeTimingData == null){
            storeTimingData = new ArrayList<>();

            StoreTimingList storeTimingList = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Monday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setStoreId(null);
            storeTimingList.setIsStoreClose(false);

            StoreTimingList storeTimingListTues = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Tuesday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);

            StoreTimingList storeTimingListWed = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Wednesday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);


            StoreTimingList storeTimingListThurs = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Thursday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);


            StoreTimingList storeTimingListFri = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Friday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);


            StoreTimingList storeTimingListSat = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Saturday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);


            StoreTimingList storeTimingListSun = new StoreTimingList();
            storeTimingList.setOpeningTiming("00:00");
            storeTimingList.setClosingTiming("23:40");
            storeTimingList.setDay("Sunday");
            storeTimingList.setStoreCloseReason("");
            storeTimingList.setIsStoreClose(false);

            storeTimingData.add(0,storeTimingList);
            storeTimingData.add(1,storeTimingListTues);
            storeTimingData.add(2,storeTimingListWed);
            storeTimingData.add(3,storeTimingListThurs);
            storeTimingData.add(4,storeTimingListFri);
            storeTimingData.add(5,storeTimingListSat);
            storeTimingData.add(6,storeTimingListSun);
        }

        return storeTimingData;
    }

    public void setStoreTimingData(List<StoreTimingList> storeTimingData) {
        this.storeTimingData = storeTimingData;
    }
    public class StoreData {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("StoreTypeId")
        @Expose
        private Integer storeTypeId;
        @SerializedName("StoreName")
        @Expose
        private String storeName;
        @SerializedName("StoreOpeningTime")
        @Expose
        private Object storeOpeningTime;
        @SerializedName("StoreClosingTime")
        @Expose
        private Object storeClosingTime;
        @SerializedName("StoreDeliveryEstimatedTime")
        @Expose
        private String storeDeliveryEstimatedTime;
        @SerializedName("StoreDeliveryFee")
        @Expose
        private Double storeDeliveryFee;
        @SerializedName("StorePhoneNumber")
        @Expose
        private String storePhoneNumber;
        @SerializedName("StoreBaseUrl")
        @Expose
        private String storeBaseUrl;
        @SerializedName("StoreImage")
        @Expose
        private String storeImage;
        @SerializedName("StoreAddress")
        @Expose
        private String storeAddress;
        @SerializedName("StoreLocationTypeId")
        @Expose
        private String storeLocationTypeId;
        @SerializedName("StoreLocationType")
        @Expose
        private String storeLocationType;
        @SerializedName("StoreLatitude")
        @Expose
        private Double storeLatitude;
        @SerializedName("StoreLongitude")
        @Expose
        private Double storeLongitude;
        @SerializedName("OrderBy")
        @Expose
        private Integer orderBy;
        @SerializedName("IsStoreOpen")
        @Expose
        private Boolean isStoreOpen;
        @SerializedName("IsDefault")
        @Expose
        private Boolean isDefault;
        @SerializedName("Status")
        @Expose
        private Boolean status;
        @SerializedName("CreateDate")
        @Expose
        private String createDate;
        @SerializedName("PhoneNo")
        @Expose
        private Object phoneNo;
        @SerializedName("CreateBy")
        @Expose
        private Integer createBy;
        @SerializedName("AdminId")
        @Expose
        private Integer adminId;
        @SerializedName("IsDeleted")
        @Expose
        private Boolean isDeleted;
        @SerializedName("IsServiceChargesEnable")
        @Expose
        private Boolean isServiceChargesEnable;
        @SerializedName("ServiceCharges")
        @Expose
        private Object serviceCharges;
        @SerializedName("Tip")
        @Expose
        private Boolean tip;
        @SerializedName("MinimumOrderLimit")
        @Expose
        private Double minimumOrderLimit;
        @SerializedName("StoreLogo")
        @Expose
        private String storeLogo;
        @SerializedName("tbl_Category")
        @Expose
        private List<Object> tblCategory = null;
        @SerializedName("tbl_OrderMaster")
        @Expose
        private List<Object> tblOrderMaster = null;
        @SerializedName("tbl_OrderReviews")
        @Expose
        private List<Object> tblOrderReviews = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getStoreTypeId() {
            return storeTypeId;
        }

        public void setStoreTypeId(Integer storeTypeId) {
            this.storeTypeId = storeTypeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public Object getStoreOpeningTime() {
            return storeOpeningTime;
        }

        public void setStoreOpeningTime(Object storeOpeningTime) {
            this.storeOpeningTime = storeOpeningTime;
        }

        public Object getStoreClosingTime() {
            return storeClosingTime;
        }

        public void setStoreClosingTime(Object storeClosingTime) {
            this.storeClosingTime = storeClosingTime;
        }

        public String getStoreDeliveryEstimatedTime() {
            return storeDeliveryEstimatedTime;
        }

        public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
            this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
        }

        public Double getStoreDeliveryFee() {
            return storeDeliveryFee;
        }

        public void setStoreDeliveryFee(Double storeDeliveryFee) {
            this.storeDeliveryFee = storeDeliveryFee;
        }

        public String getStorePhoneNumber() {
            return storePhoneNumber;
        }

        public void setStorePhoneNumber(String storePhoneNumber) {
            this.storePhoneNumber = storePhoneNumber;
        }

        public String getStoreBaseUrl() {
            return storeBaseUrl;
        }

        public void setStoreBaseUrl(String storeBaseUrl) {
            this.storeBaseUrl = storeBaseUrl;
        }

        public String getStoreImage() {
            return storeImage;
        }

        public void setStoreImage(String storeImage) {
            this.storeImage = storeImage;
        }

        public String getStoreAddress() {
            return storeAddress;
        }

        public void setStoreAddress(String storeAddress) {
            this.storeAddress = storeAddress;
        }

        public String getStoreLocationTypeId() {
            return storeLocationTypeId;
        }

        public void setStoreLocationTypeId(String storeLocationTypeId) {
            this.storeLocationTypeId = storeLocationTypeId;
        }

        public String getStoreLocationType() {
            return storeLocationType;
        }

        public void setStoreLocationType(String storeLocationType) {
            this.storeLocationType = storeLocationType;
        }

        public Double getStoreLatitude() {
            return storeLatitude;
        }

        public void setStoreLatitude(Double storeLatitude) {
            this.storeLatitude = storeLatitude;
        }

        public Double getStoreLongitude() {
            return storeLongitude;
        }

        public void setStoreLongitude(Double storeLongitude) {
            this.storeLongitude = storeLongitude;
        }

        public Integer getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(Integer orderBy) {
            this.orderBy = orderBy;
        }

        public Boolean getIsStoreOpen() {
            return isStoreOpen;
        }

        public void setIsStoreOpen(Boolean isStoreOpen) {
            this.isStoreOpen = isStoreOpen;
        }

        public Boolean getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(Boolean isDefault) {
            this.isDefault = isDefault;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public Object getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(Object phoneNo) {
            this.phoneNo = phoneNo;
        }

        public Integer getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Integer createBy) {
            this.createBy = createBy;
        }

        public Integer getAdminId() {
            return adminId;
        }

        public void setAdminId(Integer adminId) {
            this.adminId = adminId;
        }

        public Boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public Boolean getIsServiceChargesEnable() {
            return isServiceChargesEnable;
        }

        public void setIsServiceChargesEnable(Boolean isServiceChargesEnable) {
            this.isServiceChargesEnable = isServiceChargesEnable;
        }

        public Object getServiceCharges() {
            return serviceCharges;
        }

        public void setServiceCharges(Object serviceCharges) {
            this.serviceCharges = serviceCharges;
        }

        public Boolean getTip() {
            return tip;
        }

        public void setTip(Boolean tip) {
            this.tip = tip;
        }

        public Double getMinimumOrderLimit() {
            return minimumOrderLimit;
        }

        public void setMinimumOrderLimit(Double minimumOrderLimit) {
            this.minimumOrderLimit = minimumOrderLimit;
        }

        public String getStoreLogo() {
            return storeLogo;
        }

        public void setStoreLogo(String storeLogo) {
            this.storeLogo = storeLogo;
        }

        public List<Object> getTblCategory() {
            return tblCategory;
        }

        public void setTblCategory(List<Object> tblCategory) {
            this.tblCategory = tblCategory;
        }

        public List<Object> getTblOrderMaster() {
            return tblOrderMaster;
        }

        public void setTblOrderMaster(List<Object> tblOrderMaster) {
            this.tblOrderMaster = tblOrderMaster;
        }

        public List<Object> getTblOrderReviews() {
            return tblOrderReviews;
        }

        public void setTblOrderReviews(List<Object> tblOrderReviews) {
            this.tblOrderReviews = tblOrderReviews;
        }

        public Boolean getStoreOpen() {
            return isStoreOpen;
        }

        public void setStoreOpen(Boolean storeOpen) {
            isStoreOpen = storeOpen;
        }

        public Boolean getDefault() {
            return isDefault;
        }

        public void setDefault(Boolean aDefault) {
            isDefault = aDefault;
        }

        public Boolean getDeleted() {
            return isDeleted;
        }

        public void setDeleted(Boolean deleted) {
            isDeleted = deleted;
        }

        public Boolean getServiceChargesEnable() {
            return isServiceChargesEnable;
        }

        public void setServiceChargesEnable(Boolean serviceChargesEnable) {
            isServiceChargesEnable = serviceChargesEnable;
        }
    }
    public static class StoreTagsList {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("TagId")
        @Expose
        private Integer tagId;
        @SerializedName("StoreId")
        @Expose
        private Integer storeId;
        @SerializedName("Name")
        @Expose
        private String name;


        public StoreTagsList(Integer id,Integer tagId, Integer storeId, String name) {
            this.id = id;
            this.tagId = tagId;
            this.storeId = storeId;
            this.name = name;
        }



        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getTagId() {
            return tagId;
        }

        public void setTagId(Integer tagId) {
            this.tagId = tagId;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public class StoreTimingList {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("StoreId")
        @Expose
        private Integer storeId;
        @SerializedName("Day")
        @Expose
        private String day;
        @SerializedName("OpeningTiming")
        @Expose
        private String openingTiming;
        @SerializedName("ClosingTiming")
        @Expose
        private String closingTiming;
        @SerializedName("IsStoreClose")
        @Expose
        private Boolean isStoreClose;
        @SerializedName("StoreCloseReason")
        @Expose
        private String storeCloseReason;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getOpeningTiming() {
            return openingTiming;
        }

        public void setOpeningTiming(String openingTiming) {
            this.openingTiming = openingTiming;
        }

        public String getClosingTiming() {
            return closingTiming;
        }

        public void setClosingTiming(String closingTiming) {
            this.closingTiming = closingTiming;
        }

        public Boolean getIsStoreClose() {
            return isStoreClose;
        }

        public void setIsStoreClose(Boolean isStoreClose) {
            this.isStoreClose = isStoreClose;
        }

        public String getStoreCloseReason() {
            return storeCloseReason;
        }

        public void setStoreCloseReason(String storeCloseReason) {
            this.storeCloseReason = storeCloseReason;
        }

    }
}
