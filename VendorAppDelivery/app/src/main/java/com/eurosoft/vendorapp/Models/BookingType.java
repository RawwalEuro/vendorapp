package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BookingType implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("tbl_OrderMaster")
    @Expose
    private List<Object> tblOrderMaster = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<Object> getTblOrderMaster() {
        return tblOrderMaster;
    }

    public void setTblOrderMaster(List<Object> tblOrderMaster) {
        this.tblOrderMaster = tblOrderMaster;
    }

}
