package com.eurosoft.vendorapp;

import static com.eurosoft.vendorapp.Constants.FireBaseOrderParameter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vendorapp.Adapter.AdapterOrder;
import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewOrder extends AppCompatActivity {

    private FirebaseDatabase mFirebaseDatabase;
    private TextView orderIdView,orderDateView,totalAmountView,orderAddressView;
    private RecyclerView recyclerView;
    private AdapterOrder adapterOrder;
    private OrderDataModel orderDataModel;
    private LoginResponse vendorObj;
    private ApiInterface apiService;
    private RelativeLayout rlProgressBar,mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initViews();
        setOnClickListener();

    }
    public void initViews(){
        try {
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            orderIdView=(TextView) findViewById(R.id.orderId);
            orderDateView=(TextView) findViewById(R.id.orderDate);
            totalAmountView=(TextView) findViewById(R.id.totalAmount);
            orderAddressView=(TextView) findViewById(R.id.orderAddress);
            recyclerView=(RecyclerView) findViewById(R.id.rvOrderList);
            rlProgressBar = findViewById(R.id.rlProgressBar);
            mainRl = findViewById(R.id.mainRl);
            circularProgressBar = findViewById(R.id.circularProgressBar);
            progressLoader = new ProgressLoader(ActivityNewOrder.this,rlProgressBar, circularProgressBar, mainRl);
            progressLoader.progressVisiblityGone();
            Intent intent = getIntent();
            apiService  = APIClient.getClient().create(ApiInterface.class);
            orderDataModel = intent.getParcelableExtra("order_detail");
            orderIdView.setText("Order# :"+orderDataModel.getOrderNo());
            orderDateView.setText(""+orderDataModel.getOrderDate());
            totalAmountView.setText("£"+orderDataModel.getTotalAmount());
            orderAddressView.setText(""+orderDataModel.getDropoffAddress());
            vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
            setupRecycleview(orderDataModel.getStoreList().get(0).getProductList());
        }
        catch (Exception e){
            Log.e("ExceptionTag",""+e.getMessage());
        }

    }

    public void deleteDataFromFireBase() {
        Query applesQuery = mFirebaseDatabase.getReference().child("1").child(vendorObj.getId().toString());
        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot mdataSnapshot : dataSnapshot.getChildren()) {
                    mdataSnapshot.getRef().child(FireBaseOrderParameter).removeValue();
                }
                Constants.OrderIDJOFFER=0;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    private void setOnClickListener(){
        findViewById(R.id.accept_order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiForOrderStatus(Constants.Accepted,orderDataModel.getId());
            }
        });

        findViewById(R.id.reject_order).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiForOrderStatus(Constants.Cancelled,orderDataModel.getId());

            }
        });

    }
    private void setupRecycleview(List<OrderDataModel.Product> list){
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivityNewOrder.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);

        adapterOrder = new AdapterOrder(this, list, new AdapterOrder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        });

        recyclerView.setAdapter(adapterOrder);
        adapterOrder.notifyDataSetChanged();
    }

    private void callApiForOrderStatus(int statusId,int orderId){
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;
        }
        Call<WebResponse> call = apiService.getOrderStatus(orderId+"",vendorObj.getId().toString(),statusId,vendorObj.getToken()+"");
        call.enqueue(new Callback<WebResponse>() {
            @Override
            public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                        progressLoader.progressVisiblityGone();
                        deleteDataFromFireBase();
                        startActivity(new Intent(ActivityNewOrder.this,HomeActivty.class));
                        Toast.makeText(ActivityNewOrder.this, "" + message, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(ActivityNewOrder.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivityNewOrder.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivityNewOrder.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        deleteDataFromFireBase();
    }
}