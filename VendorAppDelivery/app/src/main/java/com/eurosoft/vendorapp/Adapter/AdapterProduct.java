package com.eurosoft.vendorapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Constants;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.R;
import com.fxn.stash.Stash;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ViewHolder> {

    private AdapterProduct.OnItemClickListener mOnItemClickListener;
    private List<OrderDataModel.Product> productList;
    private Context context;
    DecimalFormat df;
    String productDetail = "";
    private GetAppSettings appSettings;


    public AdapterProduct(Context ctx, List<OrderDataModel.Product> list, AdapterProduct.OnItemClickListener onItemClickListener) {
        context = ctx;
        productList = list;
        mOnItemClickListener = onItemClickListener;
        df = new DecimalFormat("0.00");
        appSettings = (GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);

    }

    public interface OnItemClickListener {
        public void onItemClick(View view, StoreList storeList);
    }

    @NonNull
    @Override
    public AdapterProduct.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_product_list, parent, false);
        AdapterProduct.ViewHolder vh = new AdapterProduct.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterProduct.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        OrderDataModel.Product product = productList.get(position);
        holder.Item.setText(product.getQuantity().intValue() + " x " + product.getProductName());
        holder.Price.setText(appSettings.getRegionSettingData().getCurrencySymbol()+df.format(product.getTotalPrice()));
        if (product.getOrderDetailOptions() != null) {
            for (int i = 0; i < product.getOrderDetailOptions().size(); i++) {
                productDetail += "" + product.getOrderDetailOptions().get(i).getOptionValue() + ", ";
            }
            holder.productDetail.setText("(" + removeLastComma(productDetail));
            holder.productDetail.setVisibility(View.VISIBLE);
        } else {
            holder.productDetail.setVisibility(View.GONE);
        }
        int pos=holder.getAdapterPosition()+1;
        holder.itemlist1.setText("Items " +pos);
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView Item, Price, productDetail, itemlist1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Item = itemView.findViewById(R.id.item1);
            Price = itemView.findViewById(R.id.item_price1);
            productDetail = itemView.findViewById(R.id.product_details);
            itemlist1 = itemView.findViewById(R.id.itemlist1);
        }
    }

    public String removeLastComma(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 2) == ',') {
            str = str.substring(0, str.length() - 2);
            str += ")";
        }
        return str;
    }
}
