package com.eurosoft.vendorapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eurosoft.vendorapp.Adapter.AdapterStoreList;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityStoreList extends AppCompatActivity {


    private RecyclerView recyclerView;
    private ApiInterface apiService;
    private LoginResponse vendorObj;
    private RelativeLayout rlProgressBar, mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;
    private  AdapterStoreList adapterStoreList;
    private  ArrayList<StoreList> list=new ArrayList<>();
    private EditText searchEt,countEt;
    private ImageView nav_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initView();
    }
    private void callApiForStoreList(String userId) {
        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(ActivityStoreList.this, "Please check your Internet", Toast.LENGTH_LONG).show();
            progressLoader.progressVisiblityGone();
            return;
        }
        Call<WebResponseList<StoreList>> call = apiService.getStoreList(userId, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponseList<StoreList>>() {
            @Override
            public void onResponse(Call<WebResponseList<StoreList>> call, Response<WebResponseList<StoreList>> response) {

                try {
                    WebResponseList webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                        list=webResponse.getData();
                      //  Toast.makeText(ActivityStoreList.this, "" + message, Toast.LENGTH_LONG).show();
                        setupRecyclerView();
                        progressLoader.progressVisiblityGone();

                    } else {
                      //  Toast.makeText(ActivityStoreList.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivityStoreList.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<StoreList>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivityStoreList.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void initView(){
        recyclerView=findViewById(R.id.recycleView);
        searchEt=findViewById(R.id.searchEt);
        countEt=findViewById(R.id.countEt);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        nav_icon = findViewById(R.id.nav_icon);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressLoader = new ProgressLoader(ActivityStoreList.this, rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityVisible();
        apiService = APIClient.getClient().create(ApiInterface.class);
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        callApiForStoreList(vendorObj.getId()+"");


        nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        countEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String countInString=s.toString();
                if(isInteger(countInString,3)){
                    int count= Integer.parseInt(countInString);
                    filterCount(count);
                }
                else {
                    Toast.makeText(ActivityStoreList.this,"Invalid Count",Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    private void setupRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivityStoreList.this, LinearLayoutManager.VERTICAL, false));
         adapterStoreList=new AdapterStoreList(this, list, new AdapterStoreList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, StoreList storeList) {
                   switch (view.getId()){
                       case R.id.editStore:
                           Intent intent=new Intent(ActivityStoreList.this, ActivtyStoreInfo.class);
                           intent.putExtra("List", (Parcelable) storeList);
                           startActivity(intent);
                           break;
                   }

            }
        });
        recyclerView.setAdapter(adapterStoreList);
        adapterStoreList.notifyDataSetChanged();
    }

    private void filter(String text) {
        ArrayList<StoreList> filteredList = new ArrayList<>();
        if (list != null) {
            for (StoreList item : list) {
                if (item.getStoreName().toLowerCase().startsWith(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        if (adapterStoreList != null) {
            adapterStoreList.filterList(filteredList);
        }
    }


    private void filterCount(int count){
        if(count==0){
            filter("null");
            return;
        }
        else if(count==list.size() || count<list.size()){
         //   filterbyRow(count);
        }
        else {
            filter("null");
            Toast.makeText(ActivityStoreList.this,"Invalid Count",Toast.LENGTH_SHORT).show();
        }

    }

    public static boolean isInteger(String s, int radix) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

}