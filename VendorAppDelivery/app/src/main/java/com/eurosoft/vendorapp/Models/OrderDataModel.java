package com.eurosoft.vendorapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDataModel  implements Parcelable {

    public static OrderDataModel newInstance() {
        return new OrderDataModel();
    }

    private OrderDataModel() {

    }

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("OrderTime")
    @Expose
    private String orderTime;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;

    public Boolean getCustomerSignature() {
        return IsCustomerSignature;
    }

    public void setCustomerSignature(Boolean customerSignature) {
        IsCustomerSignature = customerSignature;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String customerNo) {
        CustomerNo = customerNo;
    }

    public static Creator<OrderDataModel> getCREATOR() {
        return CREATOR;
    }

    @SerializedName("CustomerPhoneNumber")
    @Expose
    private String CustomerNo;

    @SerializedName("DropoffAddress")
    @Expose
    private String dropoffAddress;
    @SerializedName("DropoffLocationTypeId")
    @Expose
    private String dropoffLocationTypeId;
    @SerializedName("DropoffLocationType")
    @Expose
    private String dropoffLocationType;
    @SerializedName("DropoffLatitude")
    @Expose
    private Double dropoffLatitude;
    @SerializedName("DropoffLongitude")
    @Expose
    private Double dropoffLongitude;
    @SerializedName("SubTotal")
    @Expose
    private Double subTotal;
    @SerializedName("ShippingCharges")
    @Expose
    private Double shippingCharges;
    @SerializedName("TotalAmount")
    @Expose
    private String totalAmount;

    public void setPaymentTypeId(Integer paymentTypeId) {
        PaymentTypeId = paymentTypeId;
    }

    public void setPaymentStatusId(Integer paymentStatusId) {
        PaymentStatusId = paymentStatusId;
    }

    public int getPaymentTypeId() {
        return PaymentTypeId;
    }

    public void setPaymentTypeId(int paymentTypeId) {
        PaymentTypeId = paymentTypeId;
    }

    @SerializedName("PaymentTypeId")
    @Expose
    private Integer PaymentTypeId;
    @SerializedName("PaymentStatusId")
    @Expose
    private Integer PaymentStatusId;
    @SerializedName("PaymentTypeName")
    @Expose
    private String paymentTypeName;
    @SerializedName("DriverId")
    @Expose
    private Object driverId;
    @SerializedName("DriverName")
    @Expose
    private String driverName;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("IsCustomerSignature")
    @Expose
    private Boolean IsCustomerSignature = false;
    @SerializedName("IsDoorStepSnapEnable")
    @Expose
    private Boolean IsOnDoorStep = false;
    @SerializedName("StoreList")
    @Expose
    private List<Store> storeList = null;
    @SerializedName("ServicesCharges")
    @Expose
    private Double servicesCharges;
    @SerializedName("TipCharges")
    @Expose
    private Double TipCharges;

    public Double getServicesCharges() {
        return servicesCharges;
    }

    public Double getTipCharges() {
        return TipCharges;
    }

    public void setServicesCharges(Double servicesCharges) {
        this.servicesCharges = servicesCharges;
    }

    public void setTipCharges(Double tipCharges) {
        TipCharges = tipCharges;
    }

    public Boolean getOnDoorStep() {
        return IsOnDoorStep;
    }

    public void setOnDoorStep(Boolean onDoorStep) {
        IsOnDoorStep = onDoorStep;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    @SerializedName("DeliveryTime")
    @Expose
    private String deliveryTime;
    @SerializedName("Instruction")
    @Expose
    private String instruction;

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public OrderDataModel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
            PaymentTypeId = null;
            PaymentStatusId = null;

        } else {
            id = in.readInt();
            PaymentTypeId = in.readInt();
            PaymentStatusId = in.readInt();
        }
        customerName = in.readString();
        orderNo = in.readString();
        orderTime = in.readString();
        orderDate = in.readString();
        totalAmount = in.readString();
        IsCustomerSignature = in.readByte() != 0;     //myBoolean == true if byte != 0
        IsOnDoorStep = in.readByte() != 0;     //myBoolean == true if byte != 0
        dropoffAddress = in.readString();
        CustomerNo = in.readString();
        dropoffLocationTypeId = in.readString();
        dropoffLocationType = in.readString();
        if (in.readByte() == 0) {
            dropoffLatitude = null;
        } else {
            dropoffLatitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            dropoffLongitude = null;
        } else {
            dropoffLongitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            subTotal = null;
        } else {
            subTotal = in.readDouble();
        }
        if (in.readByte() == 0) {
            shippingCharges = null;
        } else {
            shippingCharges = in.readDouble();
        }
        if(in.readByte() == 0){
            servicesCharges=null;
        }
        else {
            servicesCharges = in.readDouble();
        }

        if(in.readByte() == 0){
            TipCharges=null;
        }
        else {
            TipCharges = in.readDouble();
        }


        paymentTypeName = in.readString();
        driverName = in.readString();
        orderStatus = in.readString();
        deliveryTime = in.readString();
        instruction = in.readString();
        storeList = in.createTypedArrayList(Store.CREATOR);

    }

    public static final Creator<OrderDataModel> CREATOR = new Creator<OrderDataModel>() {
        @Override
        public OrderDataModel createFromParcel(Parcel in) {
            return new OrderDataModel(in);
        }

        @Override
        public OrderDataModel[] newArray(int size) {
            return new OrderDataModel[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public String getDropoffLocationTypeId() {
        return dropoffLocationTypeId;
    }

    public void setDropoffLocationTypeId(String dropoffLocationTypeId) {
        this.dropoffLocationTypeId = dropoffLocationTypeId;
    }

    public String getDropoffLocationType() {
        return dropoffLocationType;
    }

    public void setDropoffLocationType(String dropoffLocationType) {
        this.dropoffLocationType = dropoffLocationType;
    }

    public Double getDropoffLatitude() {
        return dropoffLatitude;
    }

    public void setDropoffLatitude(Double dropoffLatitude) {
        this.dropoffLatitude = dropoffLatitude;
    }

    public Double getDropoffLongitude() {
        return dropoffLongitude;
    }

    public void setDropoffLongitude(Double dropoffLongitude) {
        this.dropoffLongitude = dropoffLongitude;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public Object getDriverId() {
        return driverId;
    }

    public void setDriverId(Object driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Integer getPaymentStatusId() {
        return PaymentStatusId;
    }

    public void setPaymentStatusId(int paymentStatusId) {
        PaymentStatusId = paymentStatusId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
            dest.writeInt(PaymentTypeId);
            dest.writeInt(PaymentStatusId);

        }
        dest.writeString(customerName);
        dest.writeString(orderNo);
        dest.writeString(orderTime);
        dest.writeString(orderDate);
        dest.writeString(totalAmount);
        dest.writeByte((byte) (IsCustomerSignature ? 1 : 0));
        dest.writeByte((byte) (IsOnDoorStep ? 1 : 0));
        dest.writeString(dropoffAddress);
        dest.writeString(CustomerNo);
        dest.writeString(dropoffLocationTypeId);
        dest.writeString(dropoffLocationType);

        if (dropoffLatitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(dropoffLatitude);
        }
        if (dropoffLongitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(dropoffLongitude);
        }
        if (subTotal == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(subTotal);
        }
        if (shippingCharges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(shippingCharges);
        }

        if (servicesCharges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(servicesCharges);
        }

        if (TipCharges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(TipCharges);
        }
        dest.writeString(paymentTypeName);
        dest.writeString(driverName);
        dest.writeString(orderStatus);
        dest.writeString(deliveryTime);
        dest.writeString(instruction);
        dest.writeTypedList(storeList);

    }


    public static class Store implements Parcelable {

        @SerializedName("StoreId")
        @Expose
        private Integer storeId;
        @SerializedName("StoreName")
        @Expose
        private String storeName;
        @SerializedName("StoreAddress")
        @Expose
        private String storeAddress;
        @SerializedName("StoreLocationTypeId")
        @Expose
        private String storeLocationTypeId;
        @SerializedName("StoreLocationType")
        @Expose
        private String storeLocationType;

        public String getStorePhoneNumber() {
            return StorePhoneNumber;
        }

        public static Creator<Store> getCREATOR() {
            return CREATOR;
        }

        @SerializedName("StorePhoneNumber")
        @Expose
        private String StorePhoneNumber;
        @SerializedName("StoreLatitude")
        @Expose
        private Double storeLatitude;
        @SerializedName("StoreLongitude")
        @Expose
        private Double storeLongitude;
        @SerializedName("StoreStatus")
        @Expose
        private String storeStatus;
        @SerializedName("ProductList")
        @Expose
        private List<Product> productList = null;

        protected Store(Parcel in) {
            if (in.readByte() == 0) {
                storeId = null;
            } else {
                storeId = in.readInt();
            }
            storeName = in.readString();
            storeAddress = in.readString();
            StorePhoneNumber=in.readString();
            storeLocationTypeId = in.readString();
            storeLocationType = in.readString();
            if (in.readByte() == 0) {
                storeLatitude = null;
            } else {
                storeLatitude = in.readDouble();
            }
            if (in.readByte() == 0) {
                storeLongitude = null;
            } else {
                storeLongitude = in.readDouble();
            }
            storeStatus = in.readString();
            productList = in.createTypedArrayList(Product.CREATOR);
        }

        public static final Creator<Store> CREATOR = new Creator<Store>() {
            @Override
            public Store createFromParcel(Parcel in) {
                return new Store(in);
            }

            @Override
            public Store[] newArray(int size) {
                return new Store[size];
            }
        };

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getStoreAddress() {
            return storeAddress;
        }

        public void setStoreAddress(String storeAddress) {
            this.storeAddress = storeAddress;
        }

        public String getStoreLocationTypeId() {
            return storeLocationTypeId;
        }

        public void setStoreLocationTypeId(String storeLocationTypeId) {
            this.storeLocationTypeId = storeLocationTypeId;
        }

        public String getStoreLocationType() {
            return storeLocationType;
        }

        public void setStoreLocationType(String storeLocationType) {
            this.storeLocationType = storeLocationType;
        }

        public Double getStoreLatitude() {
            return storeLatitude;
        }

        public void setStoreLatitude(Double storeLatitude) {
            this.storeLatitude = storeLatitude;
        }

        public Double getStoreLongitude() {
            return storeLongitude;
        }

        public void setStoreLongitude(Double storeLongitude) {
            this.storeLongitude = storeLongitude;
        }

        public String getStoreStatus() {
            return storeStatus;
        }

        public void setStoreStatus(String storeStatus) {
            this.storeStatus = storeStatus;
        }

        public List<Product> getProductList() {
            return productList;
        }

        public void setProductList(List<Product> productList) {
            this.productList = productList;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (storeId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(storeId);
            }
            dest.writeString(storeName);
            dest.writeString(storeAddress);
            dest.writeString(StorePhoneNumber);
            dest.writeString(storeLocationTypeId);
            dest.writeString(storeLocationType);
            if (storeLatitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(storeLatitude);
            }
            if (storeLongitude == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(storeLongitude);
            }
            dest.writeString(storeStatus);
            dest.writeTypedList(productList);
        }

    }

    public static class Product implements Parcelable {
        @SerializedName("OrderDetailId")
        @Expose
        private Integer orderDetailId;
        @SerializedName("OrderMasterId")
        @Expose
        private Integer orderMasterId;
        @SerializedName("StoreId")
        @Expose
        private Integer storeId;
        @SerializedName("ProductId")
        @Expose
        private Integer productId;
        @SerializedName("ProductName")
        @Expose
        private String productName;
        @SerializedName("Price")
        @Expose
        private Double price;
        @SerializedName("Quantity")
        @Expose
        private Double quantity;
        @SerializedName("TotalPrice")
        @Expose
        private Double totalPrice;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("OrderOptionDetailDataCount")
        @Expose
        private int orderOptionDetailDataCount;
        @SerializedName("OrderDetailOptions")
        @Expose
        private List<OrderDetailOption> orderDetailOptions;

        public Integer getOrderDetailId() {
            return orderDetailId;
        }

        public void setOrderDetailId(Integer orderDetailId) {
            this.orderDetailId = orderDetailId;
        }

        public Integer getOrderMasterId() {
            return orderMasterId;
        }

        public void setOrderMasterId(Integer orderMasterId) {
            this.orderMasterId = orderMasterId;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getQuantity() {
            return quantity;
        }

        public void setQuantity(Double quantity) {
            this.quantity = quantity;
        }

        public Double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getOrderOptionDetailDataCount() {
            return orderOptionDetailDataCount;
        }

        public void setOrderOptionDetailDataCount(int orderOptionDetailDataCount) {
            this.orderOptionDetailDataCount = orderOptionDetailDataCount;
        }

        public List<OrderDetailOption> getOrderDetailOptions() {
            return orderDetailOptions;
        }

        public void setOrderDetailOptions(List<OrderDetailOption> orderDetailOptions) {
            this.orderDetailOptions = orderDetailOptions;
        }

        public static Creator<Product> getCREATOR() {
            return CREATOR;
        }

        protected Product(Parcel in) {
            if (in.readByte() == 0) {
                orderDetailId = null;
            } else {
                orderDetailId = in.readInt();
            }
            if (in.readByte() == 0) {
                orderMasterId = null;
            } else {
                orderMasterId = in.readInt();
            }
            if (in.readByte() == 0) {
                storeId = null;
            } else {
                storeId = in.readInt();
            }
            if (in.readByte() == 0) {
                productId = null;
            } else {
                productId = in.readInt();
            }
            productName = in.readString();
            if (in.readByte() == 0) {
                price = null;
            } else {
                price = in.readDouble();
            }
            if (in.readByte() == 0) {
                quantity = null;
            } else {
                quantity = in.readDouble();
            }
            if (in.readByte() == 0) {
                totalPrice = null;
            } else {
                totalPrice = in.readDouble();
            }
            status = in.readString();
            orderOptionDetailDataCount = in.readInt();

            orderDetailOptions = in.createTypedArrayList(OrderDetailOption.CREATOR);

        }


        public static final Creator<Product> CREATOR = new Creator<Product>() {
            @Override
            public Product createFromParcel(Parcel in) {
                return new Product(in);
            }

            @Override
            public Product[] newArray(int size) {
                return new Product[size];
            }
        };


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (orderDetailId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(orderDetailId);
            }
            if (orderMasterId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(orderMasterId);
            }
            if (storeId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(storeId);
            }
            if (productId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(productId);
            }
            dest.writeString(productName);
            if (price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(price);
            }
            if (quantity == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(quantity);
            }
            if (totalPrice == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(totalPrice);
            }
            dest.writeString(status);

            dest.writeInt(orderOptionDetailDataCount);


            dest.writeTypedList(orderDetailOptions);

        }
    }

    public static class OrderDetailOption implements Parcelable {

        @SerializedName("OptionPrice")
        @Expose
        private Double optionPrice;
        @SerializedName("OptionValue")
        @Expose
        private String optionValue;
        @SerializedName("OptionId")
        @Expose
        private Integer optionId;
        @SerializedName("OrderDetailMasterId")
        @Expose
        private Integer orderDetailMasterId;
        @SerializedName("ProductId")
        @Expose
        private Integer productId;

        protected OrderDetailOption(Parcel in) {
            if (in.readByte() == 0) {
                optionPrice = null;
            } else {
                optionPrice = in.readDouble();
            }
            optionValue = in.readString();
            if (in.readByte() == 0) {
                optionId = null;
            } else {
                optionId = in.readInt();
            }
            if (in.readByte() == 0) {
                orderDetailMasterId = null;
            } else {
                orderDetailMasterId = in.readInt();
            }
            if (in.readByte() == 0) {
                productId = null;
            } else {
                productId = in.readInt();
            }
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (optionPrice == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(optionPrice);
            }
            dest.writeString(optionValue);
            if (optionId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(optionId);
            }
            if (orderDetailMasterId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(orderDetailMasterId);
            }
            if (productId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(productId);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<OrderDetailOption> CREATOR = new Creator<OrderDetailOption>() {
            @Override
            public OrderDetailOption createFromParcel(Parcel in) {
                return new OrderDetailOption(in);
            }

            @Override
            public OrderDetailOption[] newArray(int size) {
                return new OrderDetailOption[size];
            }
        };

        public Double getOptionPrice() {
            return optionPrice;
        }

        public void setOptionPrice(Double optionPrice) {
            this.optionPrice = optionPrice;
        }

        public String getOptionValue() {
            return optionValue;
        }

        public void setOptionValue(String optionValue) {
            this.optionValue = optionValue;
        }

        public Integer getOptionId() {
            return optionId;
        }

        public void setOptionId(Integer optionId) {
            this.optionId = optionId;
        }

        public Integer getOrderDetailMasterId() {
            return orderDetailMasterId;
        }

        public void setOrderDetailMasterId(Integer orderDetailMasterId) {
            this.orderDetailMasterId = orderDetailMasterId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

    }

}