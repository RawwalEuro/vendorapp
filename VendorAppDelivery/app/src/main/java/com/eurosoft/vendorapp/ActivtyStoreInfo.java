package com.eurosoft.vendorapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.multidex.BuildConfig;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.eurosoft.vendorapp.Adapter.AdapterStoreTiming;
import com.eurosoft.vendorapp.Adapter.AdapterTag;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.Store;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivtyStoreInfo extends AppCompatActivity {

    StoreList storeList;
    EditText storeNameEt, minLimitEt, storeEstTimeEt, phoneEt, dcEt, storeAddrssEt, store_logo_extension, store_image_extension;
    CheckBox services_chargers_enable_CB, tip_enable_CB;
    TextView services_chargers_enable_title, tip_enable_title, store_logo_title, store_image_title;
    RecyclerView recyclerView, tagview;
    private ApiInterface apiService;
    private LoginResponse vendorObj;
    private RelativeLayout rlProgressBar, mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;
    View storeInfo, storeCharge, storeTiming;
    private int SELECT_FILE = 1;
    RadioGroup radioGroup;
    Spinner storeTagEt,storeTypeTv;
    Boolean isLogo = false;
    Store store;
    ImageView storeImg, storeLogo;
    String logobitmap, imgbitmap;
    AdapterTag adapterTag;
    private boolean isServiceCharges;
    private boolean isTipEnable;
    private ImageView nav_icon;
    private AdapterStoreTiming adapterStoreList;
    private ArrayList<Store.StoreTimingList> tempList;
    private List<Store.StoreTagsList> tempList2;
    ;


    private GetAppSettings getAppSettings;
    private Boolean spinnerTouched = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_store_info);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        storeList = intent.getParcelableExtra("List");
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressLoader = new ProgressLoader(ActivtyStoreInfo.this, rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityVisible();
        apiService = APIClient.getClient().create(ApiInterface.class);
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        storeTypeTv = (Spinner) findViewById(R.id.storeTypeTv);
        storeNameEt = (EditText) findViewById(R.id.storeNameEt);
        minLimitEt = (EditText) findViewById(R.id.minLimitEt);
        storeEstTimeEt = (EditText) findViewById(R.id.storeEstTimeEt);
        phoneEt = (EditText) findViewById(R.id.phoneEt);
        dcEt = (EditText) findViewById(R.id.dcEt);
        storeAddrssEt = (EditText) findViewById(R.id.storeAddrssEt);
        nav_icon = (ImageView) findViewById(R.id.nav_icon);
        storeTagEt = (Spinner) findViewById(R.id.storeTagEt);
        radioGroup = findViewById(R.id.radioGroup);
        storeInfo = findViewById(R.id.storeInfo);
        storeCharge = findViewById(R.id.storeCharges);
        storeTiming = findViewById(R.id.storetiming);
        tagview = findViewById(R.id.tagRecycleView);
        storeInfo.setVisibility(View.VISIBLE);
        getAppSettings = (GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);
        storeCharge.setVisibility(View.GONE);
        storeTiming.setVisibility(View.GONE);
        setupSpinner();
        initChargesView();
        initTimingView();


        nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.saveNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validateFields();

                }
                catch (Exception e){

                }

            }
        });

        if (storeList != null) {
            Log.e("ActivtyStoreInfo", "storeList.getId()" + storeList.getId());
            callApiForStoreInFo("" + vendorObj.getId(), "" + storeList.getId());
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio0:
                        storeInfo.setVisibility(View.VISIBLE);
                        storeCharge.setVisibility(View.GONE);
                        storeTiming.setVisibility(View.GONE);
                        break;
                    case R.id.radio1:
                        storeInfo.setVisibility(View.GONE);
                        storeCharge.setVisibility(View.VISIBLE);
                        storeTiming.setVisibility(View.GONE);
                        break;
                    case R.id.radio2:
                        storeInfo.setVisibility(View.GONE);
                        storeCharge.setVisibility(View.GONE);
                        storeTiming.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        storeTagEt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    return true;
                }
                return false;
            }
        });

    }

    private void validateFields() {

        String storeName = storeNameEt.getText().toString().trim();
        String minLimit = minLimitEt.getText().toString().trim();
        String storeEst = storeEstTimeEt.getText().toString().trim();
        String phone = phoneEt.getText().toString().trim();
        String dc = dcEt.getText().toString().trim();
        String address = storeAddrssEt.getText().toString().trim();
        //   String storeTags = storeTagEt.getText().toString().trim();

        String storeLogo = store_logo_extension.getText().toString();
        String storeImage = store_image_extension.getText().toString();




        if (storeName.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Store Name is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (minLimit.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Min order limit is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (storeEst.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Estimated Delivery limit is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Phone is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (dc.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Delivery charges is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (address.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Address is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (storeLogo.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Store logo is not added", Toast.LENGTH_SHORT).show();
            return;
        }

        if (storeImage.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Store Image is not added", Toast.LENGTH_SHORT).show();
            return;
        }

        progressLoader.progressVisiblityVisible();

        store.getStoreData().setStoreName(storeName);
        store.getStoreData().setMinimumOrderLimit(Double.parseDouble(minLimit));
        store.getStoreData().setStoreDeliveryEstimatedTime(storeEst);
        store.getStoreData().setStoreDeliveryFee(Double.parseDouble(dc));
        store.getStoreData().setStoreAddress(address);
        store.setStoreTimingData(adapterStoreList.getListStore());
        store.getStoreData().setStoreLogo("");
        store.getStoreData().setStoreImage("");


        tempList = new ArrayList<>();
        tempList2 = new ArrayList<>();
        for (int i = 0; i < adapterStoreList.getListStore().size(); i++) {
            Store.StoreTimingList storeTiming = adapterStoreList.getListStore().get(i);
            storeTiming.setStoreId(store.getStoreData().getId());
            tempList.add(storeTiming);
        }
        adapterStoreList.setListStore(tempList);
        for (int i = 0; i < store.getStoreTagsData().size(); i++) {
            Store.StoreTagsList storeTagsList = store.getStoreTagsData().get(i);
            storeTagsList.setStoreId(storeTagsList.getStoreId());
            storeTagsList.setName(storeTagsList.getName());
            storeTagsList.setId(storeTagsList.getId());
            storeTagsList.setTagId(storeTagsList.getTagId());
            tempList2.add(storeTagsList);
        }
        store.setStoreTagsData(tempList2);
        store.getStoreData().setTip(isTipEnable);
        store.getStoreData().setServiceChargesEnable(isServiceCharges);
        callApiForSaveStoreInFo(store);

    }

    private void callApiForStoreInFo(String userId, String storeId) {
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(ActivtyStoreInfo.this, "Please check your Internet", Toast.LENGTH_LONG).show();
            progressLoader.progressVisiblityGone();
            return;
        }
        Call<WebResponse<Store>> call = apiService.getStoreInfo(userId, storeId, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponse<Store>>() {
            @Override
            public void onResponse(Call<WebResponse<Store>> call, Response<WebResponse<Store>> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();
                    store = (Store) webResponse.getData();

                    if (sucess) {
                        setStoreInfo();
                        setChargeInfo();
                        setStoreTimingInfo();
                        //  Toast.makeText(ActivityStoreList.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    } else {
                        //  Toast.makeText(ActivityStoreList.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivtyStoreInfo.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Store>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivtyStoreInfo.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    public void setStoreInfo() {
        storeNameEt.setText("" + store.getStoreData().getStoreName());
        minLimitEt.setText("" + store.getStoreData().getMinimumOrderLimit());
        storeEstTimeEt.setText("" + store.getStoreData().getStoreDeliveryEstimatedTime());
        phoneEt.setText("" + store.getStoreData().getStorePhoneNumber());
        dcEt.setText("" + store.getStoreData().getStoreDeliveryFee());
        storeAddrssEt.setText("" + store.getStoreData().getStoreAddress());
        setStoreTagView(store.getStoreTagsData());
        setupSpinnerStoreTypeList();
    }

    public void setChargeInfo() {
        services_chargers_enable_CB.setChecked(store.getStoreData().getIsServiceChargesEnable());
        tip_enable_CB.setChecked(store.getStoreData().getIsServiceChargesEnable());
        RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.ic_baseline_arrow_back_24).error(R.drawable.ic_baseline_calendar_month_24);
        Glide.with(this).load(store.getStoreData().getStoreBaseUrl() + store.getStoreData().getStoreLogo()).apply(options).into(storeLogo);
        Glide.with(this).load(store.getStoreData().getStoreBaseUrl() + store.getStoreData().getStoreImage()).apply(options).into(storeImg);
        store_image_extension.setText("" + store.getStoreData().getStoreLogo());
        store_logo_extension.setText("" + store.getStoreData().getStoreImage());

    }

    public void setStoreTimingInfo() {
        setupRecyclerView(store.getStoreTimingData());

    }

    public void initChargesView() {
        services_chargers_enable_title = findViewById(R.id.services_chargers_enable_title);
        tip_enable_title = findViewById(R.id.tip_enable_title);
        store_logo_title = findViewById(R.id.store_logo_title);
        store_image_title = findViewById(R.id.store_image_title);
        store_logo_extension = findViewById(R.id.store_logo_extension);
        store_image_extension = findViewById(R.id.store_image_extension);
        services_chargers_enable_CB = findViewById(R.id.services_chargers_enable_CB);
        tip_enable_CB = findViewById(R.id.tip_enable_CB);
        storeImg = findViewById(R.id.store_img);
        storeLogo = findViewById(R.id.store_logo);


        services_chargers_enable_CB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isServiceCharges = true;
                } else {
                    isServiceCharges = false;
                }
            }
        });

        tip_enable_CB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isTipEnable = true;
                } else {
                    isTipEnable = false;
                }
            }
        });


        findViewById(R.id.choose_store_image_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions()) {
                    isLogo = false;
                    galleryIntent();
                }

            }
        });
        findViewById(R.id.choose_store_logo_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissions()) {
                    isLogo = true;
                    galleryIntent();
                }
            }
        });
    }

    public void initTimingView() {
        recyclerView = findViewById(R.id.recycleView);
    }

    private void callApiForSaveStoreInFo(Store store) {
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(ActivtyStoreInfo.this, "Please check your Internet", Toast.LENGTH_LONG).show();
            progressLoader.progressVisiblityGone();
            return;
        }

        Call<WebResponse<String>> call = apiService.updateStoreInfo(store, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponse<String>>() {
            @Override
            public void onResponse(Call<WebResponse<String>> call, Response<WebResponse<String>> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                        Toast.makeText(ActivtyStoreInfo.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();
                        finish();

                    } else {
                        Toast.makeText(ActivtyStoreInfo.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();
                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivtyStoreInfo.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<String>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivtyStoreInfo.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void setStoreTagView(List<Store.StoreTagsList> storeTagsLists) {
        tagview.setLayoutManager(new LinearLayoutManager(ActivtyStoreInfo.this, LinearLayoutManager.HORIZONTAL, false));
        adapterTag = new AdapterTag(this, storeTagsLists, new AdapterTag.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {
                    case R.id.tag_name:
                        try {
                            removeItem(position, storeTagsLists);

                        }
                        catch (Exception e){

                        }
                }
            }
        });
        tagview.setAdapter(adapterTag);
        adapterTag.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            onSelectFromGalleryResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap thumbnail = null;
        if (data != null) {
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        getFileName(data.getData());

    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    private boolean checkAndRequestPermissions() {
        int wtite = ContextCompat.checkSelfPermission(ActivtyStoreInfo.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(ActivtyStoreInfo.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(ActivtyStoreInfo.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), SELECT_FILE);
            return false;
        }
        return true;
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String FileName = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    FileName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                    cursor.moveToFirst();
                    String fileSize = cursor.getString(sizeIndex);
                    Log.e("DocumentsActivity", "result" + FileName);
                    double m = ((Long.parseLong(fileSize) / 1024.0) / 1024.0);
                    if (IsFileSizeLimitCross(m)) {
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                            String bitmapstring = encodeTobase64(bitmap);
                            if (!isLogo) {
                                imgbitmap = bitmapstring;
                                store_image_extension.setText("" + FileName);
                            } else {
                                logobitmap = bitmapstring;
                                store_logo_extension.setText("" + FileName);
                            }
                            //   Log.e("DocumentsActivity",""+bitmapstring);

                        } catch (Exception e) {
                            Log.e("DocumentsActivity", "" + e.getMessage());
                            Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "File Size too Large to upload" + formatFileSize(Long.parseLong(fileSize)), Toast.LENGTH_SHORT).show();
                    }

                }
            } finally {
                cursor.close();
            }
        }
        if (FileName == null) {
            FileName = uri.getPath();
            int cut = FileName.lastIndexOf('/');
            if (cut != -1) {
                FileName = FileName.substring(cut + 1);
            }
        }
        return FileName;
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public static Boolean IsFileSizeLimitCross(double mb) {
        if (mb >= 5) {
            return false;
        }
        return true;
    }

    public static String formatFileSize(long size) {
        String hrSize = null;

        double b = size;
        double k = size / 1024.0;
        double m = ((size / 1024.0) / 1024.0);
        double g = (((size / 1024.0) / 1024.0) / 1024.0);
        double t = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (t > 1) {
            hrSize = dec.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dec.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dec.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(b).concat(" Bytes");
        }

        return hrSize;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                galleryIntent();
            } else {
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        } catch (Exception e) {
            Toast.makeText(ActivtyStoreInfo.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_LONG)
                .setAction(getString(actionStringId), listener).show();
    }

    private void setupRecyclerView(List<Store.StoreTimingList> storeTimingLists) {
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivtyStoreInfo.this, LinearLayoutManager.VERTICAL, false));
        adapterStoreList = new AdapterStoreTiming(this, storeTimingLists, new AdapterStoreTiming.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        recyclerView.setAdapter(adapterStoreList);
        adapterStoreList.notifyDataSetChanged();

    }

    private void removeItem(int position, List<Store.StoreTagsList> storeTagsLists) {
        storeTagsLists.remove(position);
        adapterTag.notifyItemRemoved(position);
        adapterTag.notifyItemRangeChanged(position, storeTagsLists.size());
    }

    public void setupSpinner() {
        ArrayList<String> StoreTagNameList = new ArrayList<>();
        ArrayList<GetAppSettings.StoreTag> StoreTagList = new ArrayList<>();

        for (int i = 0; i < getAppSettings.getStoreTagList().size(); i++) {
            StoreTagNameList.add(getAppSettings.getStoreTagList().get(i).getName());
            StoreTagList.add(getAppSettings.getStoreTagList().get(i));

        }
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, StoreTagNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storeTagEt.setSelected(false);
        storeTagEt.setAdapter(adapter);
        storeTagEt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                spinnerTouched = true;
                return false;
            }
        });
        storeTagEt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (spinnerTouched) {
                        Store.StoreTagsList storeTagsList=new Store.StoreTagsList(StoreTagList.get(position).getId(), StoreTagList.get(position).getId(), storeList.getId(), StoreTagNameList.get(position));
                        for(int i=0; i<store.getStoreTagsData().size(); i++){
                            if(store.getStoreTagsData().get(i).getName().equals(storeTagsList.getName())){
                                Toast.makeText(ActivtyStoreInfo.this, "Tag Already Found", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                        store.getStoreTagsData().add(storeTagsList);
                        Toast.makeText(ActivtyStoreInfo.this, "" + StoreTagNameList.get(position), Toast.LENGTH_LONG).show();
                        if(adapterTag.getTagList().size()!=0){
                            tagview.scrollToPosition(adapterTag.getTagList().size() - 1);
                        }
                        adapterTag.notifyDataSetChanged();
                    }
                }
                catch (Exception e){

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
    }


    public void setupSpinnerStoreTypeList() {
        ArrayList<String> StoreTypeNameList = new ArrayList<>();
        ArrayList<GetAppSettings.StoreType> StoreTypeList = new ArrayList<>();


        for (int i = 0; i < getAppSettings.getStoreTypeList().size(); i++) {
            StoreTypeNameList.add(getAppSettings.getStoreTypeList().get(i).getStoreTypeName()+"");
            StoreTypeList.add(getAppSettings.getStoreTypeList().get(i));
            Log.e("sssd",""+getAppSettings.getStoreTypeList().get(i).getStoreTypeName()+"");

        }
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, StoreTypeNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storeTypeTv.setAdapter(adapter);
        storeTypeTv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    store.getStoreData().setStoreTypeId(StoreTypeList.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}