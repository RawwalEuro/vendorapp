package com.eurosoft.vendorapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("UserEmail")
    @Expose
    private String userEmail;
    @SerializedName("UserPassword")
    @Expose
    private String userPassword;
    @SerializedName("UserRoles")
    @Expose
    private Integer userRoles;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("Culture")
    @Expose
    private String culture;
    @SerializedName("UserPhone")
    @Expose
    private String userPhone;
    @SerializedName("Token")
    @Expose
    private String token;



    public LoginResponse() {

    }


    public LoginResponse(Integer id, String userName, String userEmail, String userPassword, Integer userRoles, Boolean status, String createDate, String culture, String userPhone, String token) {
        this.id = id;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userRoles = userRoles;
        this.status = status;
        this.createDate = createDate;
        this.culture = culture;
        this.userPhone = userPhone;
        this.token = token;
    }

    public LoginResponse(Integer id, String userName, String userEmail, String userPassword, String culture, String userPhone) {
        this.id = id;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.culture = culture;
        this.userPhone = userPhone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Integer getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Integer userRoles) {
        this.userRoles = userRoles;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
