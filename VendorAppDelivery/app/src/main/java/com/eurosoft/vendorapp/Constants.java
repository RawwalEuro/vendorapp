package com.eurosoft.vendorapp;


import static android.provider.ContactsContract.Directory.PACKAGE_NAME;

public class Constants {

    public static final String STARTFOREGROUND_ACTION = "STARTFOREGROUND_ACTION";
    public static final String STOPFOREGROUND_ACTION = "STOPFOREGROUND_ACTION";
    public static final String BASE_URL = "https://onlinedeliverysystem.co.uk/EcommerceSystem/VendorApp/";
    public static final String TEST_URL = "https://onlinedeliverysystem.co.uk/EcommerceSystem/VendorApp/";

    public static final String LOGIN_OBJ = "LOGIN_OBJ";
    public static final String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static final String APP_SETTINGS = "APP_SETTINGS";
    public static String FireBaseOrderParameter = "Order";
    public static String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME + ".started_from_notification";
    public static final String CHANNEL_ID1 = "ForegroundServiceChannel";
    public static final int Accepted=1;
    public static final int Cancelled=0;
    public static int OrderIDJOFFER = 0;
    public static final int CustomerNameTypeID=1;
    public static final int DriverNoTypeID=2;
    public static final String AcceptPending="Accepted";
    public static final String Collect="Collected";
    public static final String go_to_customer="On Route";
    public static final String arrival="Arrived";
    public static final String deliver="Delivered";
    public static final String completed_order="Completed";
    public static final String waiting_order="Waiting";
    public static final String decline_order="Declined";




}
