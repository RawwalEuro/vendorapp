package com.eurosoft.vendorapp.Network;

import com.eurosoft.vendorapp.Models.OrderDashBoardStatusPojo;
import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.Models.SearchData;
import com.eurosoft.vendorapp.Models.Store;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.Models.UpdateStoreRequest;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Pojo.SearchResponse;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.eurosoft.vendorapp.Utils.WebResponseList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @POST("Login")
    Call<WebResponse<LoginResponse>> loginUser(@Body LoginResponse requestLogin);

    @GET("GetDashboardOrdersData")
    Call<WebResponse<OrderDashBoardStatusPojo>> dashboardAll(@Query("UserId") String userId, @Query("OrderStatus") String OrderStatus, @Header("Authorization") String auth);

    @POST("SearchOrders")
    Call<WebResponseList<Orders>> searchOrder(@Body SearchResponse searchResponse, @Header("Authorization") String auth);

    @POST("UpdateProfile")
    Call<WebResponse<LoginResponse>> updateprofile(@Body LoginResponse loginResponse, @Header("Authorization") String auth);

    @GET("GetOrderByUserIdAndOrderId")
    Call<WebResponse<OrderDataModel>> getOrderData(@Query("UserId") String userId, @Query("OrderId") String orderId, @Header("Authorization") String auth);

    @POST("AcceptRejectOrderByUserAndOrderId")
    Call<WebResponse> getOrderStatus(@Query("OrderId") String orderId,@Query("UserId") String userId,@Query("TypeId") int typeId, @Header("Authorization") String auth);

    @GET("GetAppSetting")
    Call<WebResponse<GetAppSettings>> getAppSettings(@Header("Authorization") String auth);


    @GET("SearchData")
    Call<WebResponse<SearchData>> getSearchForCustomerAndDriverNo(@Query("Prefix") String prefix,@Query("Type") int typeId,@Header("Authorization") String auth);
    @GET("ForgetPassword")
    Call<WebResponse<String>> forgetPassWord(@Query("Username") String userId);

    @GET("GetAllStoreByUserId")
    Call<WebResponseList<StoreList>> getStoreList(@Query("UserId") String userId, @Header("Authorization") String auth);

    @GET("GetStoreDetailByStoreAndUserId")
    Call<WebResponse<Store>> getStoreInfo(@Query("UserId") String userId,@Query("StoreId") String storeId, @Header("Authorization") String auth);


    @POST("UpdateStoreByStoreAndUserId")
    Call<WebResponse<String>> updateStoreInfo(@Body Store store, @Header("Authorization") String auth);


}
