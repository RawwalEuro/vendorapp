package com.eurosoft.vendorapp.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PaymentType implements Serializable {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("PaymentTypeName")
        @Expose
        private String paymentTypeName;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;
        @SerializedName("IsDeleted")
        @Expose
        private Boolean isDeleted;
        @SerializedName("tbl_OrderMaster")
        @Expose
        private List<Object> tblOrderMaster = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPaymentTypeName() {
            return paymentTypeName;
        }

        public void setPaymentTypeName(String paymentTypeName) {
            this.paymentTypeName = paymentTypeName;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

        public Boolean getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(Boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public List<Object> getTblOrderMaster() {
            return tblOrderMaster;
        }

        public void setTblOrderMaster(List<Object> tblOrderMaster) {
            this.tblOrderMaster = tblOrderMaster;


    }
}
