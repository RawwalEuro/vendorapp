package com.eurosoft.vendorapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.ViewHolder> {


    private AdapterOrder.OnItemClickListener mOnItemClickListener;
    private List<OrderDataModel.Product> listProduct;
    private Context context;
    private String productDetail="(";

    public AdapterOrder(Context ctx, List<OrderDataModel.Product> list , AdapterOrder.OnItemClickListener onItemClickListener) {
        context = ctx;
        listProduct = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_card_product, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderDataModel.Product product = listProduct.get(position);
        if(product.getOrderDetailOptions()!=null){
            for(int i=0; i<product.getOrderDetailOptions().size(); i++){
                productDetail +=""+product.getOrderDetailOptions().get(i).getOptionValue()+", ";
            }
         //   productDetail = productDetail.replace(productDetail.substring(productDetail.length()-1), "");
            holder.product_detail.setText(""+removeLastComma(productDetail));
            holder.product_detail.setVisibility(View.VISIBLE);
        }
        else {
            holder.product_detail.setVisibility(View.GONE);
        }
        holder.product_name.setText(product.getQuantity().intValue()+" x "+product.getProductName());


    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView product_name, product_detail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            product_name=itemView.findViewById(R.id.product_name);
            product_detail=itemView.findViewById(R.id.product_details);
        }
    }
    public String removeLastComma(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 2) == ',') {
            str = str.substring(0, str.length() - 2);
            str+=")";
        }
        return str;
    }

}
