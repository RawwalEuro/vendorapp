package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateStoreRequest {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("storeTypeId")
    @Expose
    private Integer storeTypeId;

    public UpdateStoreRequest(Integer id, Integer storeTypeId, String storeName, String storeDeliveryEstimatedTime, String storeDeliveryFee, String storePhoneNumber, String storeAddress, String storeAddressvalue, int storeLocationTypeId, String storeLocationType, Double storeLatitude, Double storeLongitude, Integer userId, Boolean isServiceChargesEnable, String serviceCharges, Boolean tip, Double minimumOrderLimit, String storeLogo, String storeImage, Store.StoreTagsList storeTagsData, Store.StoreTimingList storeTimings) {
        this.id = id;
        this.storeTypeId = storeTypeId;
        this.storeName = storeName;
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
        this.storeDeliveryFee = storeDeliveryFee;
        this.storePhoneNumber = storePhoneNumber;
        this.storeAddress = storeAddress;
        this.storeAddressvalue = storeAddressvalue;
        this.storeLocationTypeId = storeLocationTypeId;
        this.storeLocationType = storeLocationType;
        this.storeLatitude = storeLatitude;
        this.storeLongitude = storeLongitude;
        this.userId = userId;
        this.isServiceChargesEnable = isServiceChargesEnable;
        this.serviceCharges = serviceCharges;
        this.tip = tip;
        this.minimumOrderLimit = minimumOrderLimit;
        this.storeLogo = storeLogo;
        this.storeImage = storeImage;
        this.storeTagsData = storeTagsData;
        this.storeTimings = storeTimings;
    }

    @SerializedName("storeName")
    @Expose
    private String storeName;
    @SerializedName("storeDeliveryEstimatedTime")
    @Expose
    private String storeDeliveryEstimatedTime;
    @SerializedName("storeDeliveryFee")
    @Expose
    private String storeDeliveryFee;
    @SerializedName("storePhoneNumber")
    @Expose
    private String storePhoneNumber;
    @SerializedName("storeAddress")
    @Expose
    private String storeAddress;
    @SerializedName("storeAddressvalue")
    @Expose
    private String storeAddressvalue;
    @SerializedName("storeLocationTypeId")
    @Expose
    private int storeLocationTypeId;
    @SerializedName("storeLocationType")
    @Expose
    private String storeLocationType;
    @SerializedName("storeLatitude")
    @Expose
    private Double storeLatitude;
    @SerializedName("storeLongitude")
    @Expose
    private Double storeLongitude;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("isServiceChargesEnable")
    @Expose
    private Boolean isServiceChargesEnable;
    @SerializedName("serviceCharges")
    @Expose
    private String serviceCharges;
    @SerializedName("tip")
    @Expose
    private Boolean tip;
    @SerializedName("minimumOrderLimit")
    @Expose
    private Double minimumOrderLimit;
    @SerializedName("storeLogo")
    @Expose
    private String storeLogo;
    @SerializedName("storeImage")
    @Expose
    private String storeImage;
    @SerializedName("storeTagsData")
    @Expose
    private Store.StoreTagsList storeTagsData;
    @SerializedName("storeTimings")
    @Expose
    private Store.StoreTimingList storeTimings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoreTypeId() {
        return storeTypeId;
    }

    public void setStoreTypeId(Integer storeTypeId) {
        this.storeTypeId = storeTypeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreDeliveryEstimatedTime() {
        return storeDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    public String getStoreDeliveryFee() {
        return storeDeliveryFee;
    }

    public void setStoreDeliveryFee(String storeDeliveryFee) {
        this.storeDeliveryFee = storeDeliveryFee;
    }

    public String getStorePhoneNumber() {
        return storePhoneNumber;
    }

    public void setStorePhoneNumber(String storePhoneNumber) {
        this.storePhoneNumber = storePhoneNumber;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreAddressvalue() {
        return storeAddressvalue;
    }

    public void setStoreAddressvalue(String storeAddressvalue) {
        this.storeAddressvalue = storeAddressvalue;
    }

    public int getStoreLocationTypeId() {
        return storeLocationTypeId;
    }

    public void setStoreLocationTypeId(int storeLocationTypeId) {
        this.storeLocationTypeId = storeLocationTypeId;
    }

    public String getStoreLocationType() {
        return storeLocationType;
    }

    public void setStoreLocationType(String storeLocationType) {
        this.storeLocationType = storeLocationType;
    }

    public Double getStoreLatitude() {
        return storeLatitude;
    }

    public void setStoreLatitude(Double storeLatitude) {
        this.storeLatitude = storeLatitude;
    }

    public Double getStoreLongitude() {
        return storeLongitude;
    }

    public void setStoreLongitude(Double storeLongitude) {
        this.storeLongitude = storeLongitude;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getServiceChargesEnable() {
        return isServiceChargesEnable;
    }

    public void setServiceChargesEnable(Boolean serviceChargesEnable) {
        isServiceChargesEnable = serviceChargesEnable;
    }

    public String getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public Boolean getTip() {
        return tip;
    }

    public void setTip(Boolean tip) {
        this.tip = tip;
    }

    public Double getMinimumOrderLimit() {
        return minimumOrderLimit;
    }

    public void setMinimumOrderLimit(Double minimumOrderLimit) {
        this.minimumOrderLimit = minimumOrderLimit;
    }

    public String getStoreLogo() {
        return storeLogo;
    }

    public void setStoreLogo(String storeLogo) {
        this.storeLogo = storeLogo;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public Store.StoreTagsList getStoreTagsData() {
        return storeTagsData;
    }

    public void setStoreTagsData(Store.StoreTagsList storeTagsData) {
        this.storeTagsData = storeTagsData;
    }

    public Store.StoreTimingList getStoreTimings() {
        return storeTimings;
    }

    public void setStoreTimings(Store.StoreTimingList storeTimings) {
        this.storeTimings = storeTimings;
    }
}
