package com.eurosoft.vendorapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vendorapp.Adapter.AdapterHome;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.Models.SearchData;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Pojo.SearchResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.eurosoft.vendorapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityFilter extends AppCompatActivity {

    private RecyclerView resultRv;
    private AdapterHome adapterHome;
    // private List<String> strings = new ArrayList<>();
    private List<Orders> ordersList = new ArrayList<>();
    private EditText ordernumber;
    private AutoCompleteTextView customername,driverNumber;
    private Spinner statusSpinner;
    private Calendar calendarfromdate, calendertilldate;
    private String startdate, enddate,customerId=null,driverId=null;
    private DatePickerDialog datePickerDialogfromdate, datePickerDialogtilldate;
    private TextView startDateview, endDateview;
    Boolean fromDate = false;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    Date date1 = null;
    Date date2 = null;
    int bookingCurrentStatus = 0;
    private List<SearchData.Customer> searchCustomerData =new ArrayList<>();
    private List<SearchData.Driver> searchDriverData =new ArrayList<>();

    private LoginResponse vendorObj;
    private GetAppSettings getAppSettings;
    private ApiInterface apiService;
    private RelativeLayout rlProgressBar, mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        initViews();
        setonClickListener();
    }

    private void initViews() {
        resultRv = findViewById(R.id.resultRv);
        customername = findViewById(R.id.customerNameEt);
        ordernumber = findViewById(R.id.orderNumberEt);
        driverNumber = findViewById(R.id.driverNumberEt);
        statusSpinner = findViewById(R.id.statusEt);
        startDateview = (TextView) findViewById(R.id.startDateEt);
        endDateview = (TextView) findViewById(R.id.endDateEt);
        startDateview.setText(getCurrentDate("d MMM"));
        endDateview.setText(getCurrentDate("d MMM"));
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressLoader = new ProgressLoader(ActivityFilter.this, rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityGone();
        getCalendarInstance();
        handler = new Handler();
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        getAppSettings=(GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);
        apiService = APIClient.getClient().create(ApiInterface.class);
        setupSpinner();

    }


    private void getCalendarInstance() {
        calendarfromdate = Calendar.getInstance(TimeZone.getDefault());
        int month = calendarfromdate.get(Calendar.MONTH) + 1;
        calendertilldate = Calendar.getInstance(TimeZone.getDefault());
        startdate = "" + calendarfromdate.get(Calendar.YEAR) + "-" + month + "-" + calendarfromdate.get(Calendar.DAY_OF_MONTH) + "T00:00:00";
        enddate = "" + calendarfromdate.get(Calendar.YEAR) + "-" + month + "-" + calendarfromdate.get(Calendar.DAY_OF_MONTH) + "T00:00:00";
        datePickerDialogfromdate = new DatePickerDialog(ActivityFilter.this, R.style.MyDatePickerStyle, datePickerListener, calendarfromdate.get(Calendar.YEAR), calendarfromdate.get(Calendar.MONTH), calendarfromdate.get(Calendar.DAY_OF_MONTH));
        datePickerDialogtilldate = new DatePickerDialog(ActivityFilter.this, R.style.MyDatePickerStyle, datePickerListener, calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH));
        datePickerDialogfromdate.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialogtilldate.getDatePicker().setMaxDate(new Date().getTime());

    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            if (fromDate) {
                setFromDate(selectedDay, selectedMonth, selectedYear);
            } else {
                setTillDate(selectedDay, selectedMonth, selectedYear);
            }

        }
    };


    private void setFromDate(int selectedDay, int selectedMonth, int selectedYear) {
        calendarfromdate.set(selectedYear, selectedMonth, selectedDay);
        String DateFrom = "" + selectedDay + " " + getMonth(selectedMonth + 1).substring(0, 3);
        startDateview.setText(DateFrom);
        selectedMonth = selectedMonth + 1;
        startdate = "" + selectedYear + "-" + selectedMonth + "-" + selectedDay + "T00:00:00";
        Log.e("dATE1", "startdate" + startdate);
        Log.e("dATE1", "enddate" + enddate);
        datePickerDialogfromdate.dismiss();
    }

    private void setTillDate(int selectedDay, int selectedMonth, int selectedYear) {
        calendertilldate.set(selectedYear, selectedMonth, selectedDay);
        String DateTill = "" + selectedDay + " " + getMonth(selectedMonth + 1).substring(0, 3);
        endDateview.setText(DateTill);
        selectedMonth = selectedMonth + 1;
        enddate = "" + selectedYear + "-" + selectedMonth + "-" + selectedDay + "T00:00:00";
        Log.e("dATE1", "startdate" + startdate);
        Log.e("dATE1", "enddate" + enddate);
        datePickerDialogtilldate.dismiss();
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    public String getCurrentDate(String dateformat) {
        Date Date = new Date();
        String TodayDate = String.valueOf(DateFormat.format(dateformat, Date.getTime()));
        return TodayDate;
    }

    public void setupSpinner() {
        ArrayList<String> bookStatusListName=new ArrayList<>();
        ArrayList<Integer> bookStatusListId=new ArrayList<>();

        for(int i=0; i< getAppSettings.getOrderStatus().size(); i++){
            bookStatusListName.add(getAppSettings.getOrderStatus().get(i).getOrderStatus());
            bookStatusListId.add(getAppSettings.getOrderStatus().get(i).getId());
        }
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, bookStatusListName);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        statusSpinner.setAdapter(adapter);
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookingCurrentStatus = bookStatusListId.get(position);
                Log.e("sfsfsff",""+bookStatusListName.get(position));
            }

            public void onNothingSelected(AdapterView<?> parent) {
                bookingCurrentStatus=0;
            }
        });

    }

    private void validation() {
    /*    if (customername.getText().toString().isEmpty()) {
            Toast.makeText(ActivityFilter.this, "Customer Name is Empty", Toast.LENGTH_SHORT).show();
            return;

        }

        if (ordernumber.getText().toString().isEmpty()) {
            Toast.makeText(ActivityFilter.this, "Order Number is Empty", Toast.LENGTH_SHORT).show();
            return;

        }*/
        try {
            date1 = sdf.parse(startdate);
            date2 = sdf.parse(enddate);
            if (sdf.format(date2).compareTo(sdf.format(date1)) < 0) {
                Toast.makeText(ActivityFilter.this, "End Date is greater than Start Date", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!customername.getText().toString().isEmpty() && customerId==null){
                Toast.makeText(ActivityFilter.this, "Item isn't selected from List", Toast.LENGTH_SHORT).show();
                filter("null");
                return;
            }

            if(!driverNumber.getText().toString().isEmpty() && driverNumber==null){
                filter("null");
                Toast.makeText(ActivityFilter.this, "Item isn't selected from List", Toast.LENGTH_SHORT).show();
                return;
            }

            if(customername.getText().toString().isEmpty()){
                customerId=null;
            }

            if(driverNumber.getText().toString().isEmpty()){
                driverId=null;
            }

            callApiForFilterData(startdate, enddate,customerId, ordernumber.getText().toString(), driverId, bookingCurrentStatus);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void setonClickListener() {
        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validation();
                } catch (Exception e) {
                    Log.e("EXCEPTION_TAG", "" + e.getMessage());
                }

            }
        });
        findViewById(R.id.startDateRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDate = true;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);
            }
        });

        findViewById(R.id.startDateEt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDate = true;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);
            }
        });

        findViewById(R.id.endDateRL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDate = false;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);
            }
        });

        findViewById(R.id.endDateEt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDate = false;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);
            }
        });

        findViewById(R.id.nav_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        customername.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (handler != null) {
                    handler.removeCallbacksAndMessages(null);
                }
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callApiForSearchData(s.toString(), Constants.CustomerNameTypeID);

                    }
                }, 500);
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        customername.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                customerId=searchCustomerData.get(pos).getId()+"";
            }
        });


        driverNumber.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (handler != null) {
                    handler.removeCallbacks(null);
                }
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callApiForSearchData(s.toString(), Constants.DriverNoTypeID);
                    }
                }, 500);
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        driverNumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                driverId=searchDriverData.get(pos).getId()+"";
            }
        });


    }

    private void callApiForFilterData(String startdate, String enddate, String customerId, String orderNo, String driverId, int bookingStatusId) {
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;

        }
        SearchResponse searchResponse = new SearchResponse(startdate, enddate, customerId, null, null, driverId, null, bookingStatusId, orderNo, null, vendorObj.getId());
        Call<WebResponseList<Orders>> call = apiService.searchOrder(searchResponse, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponseList<Orders>>() {
            @Override
            public void onResponse(Call<WebResponseList<Orders>> call, Response<WebResponseList<Orders>> response) {

                try {
                    WebResponseList webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                        ordersList = webResponse.getData();
                        setupRecycleview(ordersList);
                        progressLoader.progressVisiblityGone();
                    //    Toast.makeText(ActivityFilter.this, "" + message, Toast.LENGTH_LONG).show();
                    } else {
                        filter("null");
                        Toast.makeText(ActivityFilter.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    filter("null");
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(ActivityFilter.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<Orders>> call, Throwable t) {
                filter("null");
                progressLoader.progressVisiblityGone();
                Toast.makeText(ActivityFilter.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    private void setupRecycleview(List<Orders> list) {
        resultRv.setLayoutManager(new LinearLayoutManager(ActivityFilter.this, LinearLayoutManager.VERTICAL, false));
        resultRv.setHasFixedSize(true);

        adapterHome = new AdapterHome(ActivityFilter.this, list, new AdapterHome.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position,Orders orders) {
            }
        });

        resultRv.setAdapter(adapterHome);
        adapterHome.notifyDataSetChanged();
    }

    private void filter(String text) {
        ArrayList<Orders> filteredList = new ArrayList<>();
        if (ordersList != null) {
            for (Orders item : ordersList) {
                if (item.getOrderDate().toLowerCase().startsWith(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        if (adapterHome != null) {
            adapterHome.filterList(filteredList);
        }
    }

    private void callApiForSearchData(String prefix, int typeId) {
        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(ActivityFilter.this, "Please check your Internet", Toast.LENGTH_LONG).show();
            return;
        }
        Call<WebResponse<SearchData>> call = apiService.getSearchForCustomerAndDriverNo(prefix, typeId, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponse<SearchData>>() {
            @Override
            public void onResponse(Call<WebResponse<SearchData>> call, Response<WebResponse<SearchData>> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();
                    searchCustomerData = response.body().getData().getCustomerList();
                    searchDriverData=response.body().getData().getDriverList();
                    List<String> customerNames = new ArrayList<>();
                    List<String> DriverName = new ArrayList<>();

                    if (sucess) {
                        if(typeId==1){
                            for (int i = 0; i < searchCustomerData.size(); i++) {
                                customerNames.add(searchCustomerData.get(i).getCustomerFirstName()+" "+searchCustomerData.get(i).getCustomerLastName());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, customerNames);
                            customername.setThreshold(1);
                            customername.setAdapter(adapter);
                            customername.showDropDown();


                        }
                        else {
                            for (int i = 0; i < searchDriverData.size(); i++) {
                                DriverName.add(searchDriverData.get(i).getDriverNo());
                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, DriverName);
                            driverNumber.setThreshold(1);
                            driverNumber.setAdapter(adapter);
                            driverNumber.showDropDown();

                        }

                      //  Toast.makeText(ActivityFilter.this, "" + message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ActivityFilter.this, "No Record Found" + message, Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e) {
                    Toast.makeText(ActivityFilter.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SearchData>> call, Throwable t) {
                Toast.makeText(ActivityFilter.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

}