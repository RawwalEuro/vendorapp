package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WebsiteSettingData implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("websitename")
    @Expose
    private String websitename;
    @SerializedName("websiteurl")
    @Expose
    private String websiteurl;
    @SerializedName("websiteaddress")
    @Expose
    private String websiteaddress;
    @SerializedName("websitelogo")
    @Expose
    private String websitelogo;
    @SerializedName("contactaddress")
    @Expose
    private String contactaddress;
    @SerializedName("receivingemail")
    @Expose
    private String receivingemail;
    @SerializedName("contactemail")
    @Expose
    private String contactemail;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("internationalphone")
    @Expose
    private String internationalphone;
    @SerializedName("ordernumber")
    @Expose
    private String ordernumber;
    @SerializedName("smtphost")
    @Expose
    private String smtphost;
    @SerializedName("smtpport")
    @Expose
    private Integer smtpport;
    @SerializedName("smtpemail")
    @Expose
    private String smtpemail;
    @SerializedName("smtppassword")
    @Expose
    private String smtppassword;
    @SerializedName("smtpsecureconnection")
    @Expose
    private Boolean smtpsecureconnection;
    @SerializedName("isemailsentonorder")
    @Expose
    private Boolean isemailsentonorder;
    @SerializedName("RegionId")
    @Expose
    private Integer regionId;
    @SerializedName("countrycode")
    @Expose
    private Object countrycode;
    @SerializedName("mapapi")
    @Expose
    private String mapapi;
    @SerializedName("ClientId")
    @Expose
    private Integer clientId;
    @SerializedName("IsMultipleResturentOrderAllow")
    @Expose
    private Boolean isMultipleResturentOrderAllow;
    @SerializedName("IsStripeEnable")
    @Expose
    private Object isStripeEnable;
    @SerializedName("IsJudoEnable")
    @Expose
    private Object isJudoEnable;
    @SerializedName("IsPaypalEnable")
    @Expose
    private Object isPaypalEnable;
    @SerializedName("IsCurrencySymbolEnableInCustomerApp")
    @Expose
    private Boolean isCurrencySymbolEnableInCustomerApp;
    @SerializedName("IsCurrencySymbolEnableInDriverApp")
    @Expose
    private Object isCurrencySymbolEnableInDriverApp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWebsitename() {
        return websitename;
    }

    public void setWebsitename(String websitename) {
        this.websitename = websitename;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getWebsiteaddress() {
        return websiteaddress;
    }

    public void setWebsiteaddress(String websiteaddress) {
        this.websiteaddress = websiteaddress;
    }

    public String getWebsitelogo() {
        return websitelogo;
    }

    public void setWebsitelogo(String websitelogo) {
        this.websitelogo = websitelogo;
    }

    public String getContactaddress() {
        return contactaddress;
    }

    public void setContactaddress(String contactaddress) {
        this.contactaddress = contactaddress;
    }

    public String getReceivingemail() {
        return receivingemail;
    }

    public void setReceivingemail(String receivingemail) {
        this.receivingemail = receivingemail;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInternationalphone() {
        return internationalphone;
    }

    public void setInternationalphone(String internationalphone) {
        this.internationalphone = internationalphone;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getSmtphost() {
        return smtphost;
    }

    public void setSmtphost(String smtphost) {
        this.smtphost = smtphost;
    }

    public Integer getSmtpport() {
        return smtpport;
    }

    public void setSmtpport(Integer smtpport) {
        this.smtpport = smtpport;
    }

    public String getSmtpemail() {
        return smtpemail;
    }

    public void setSmtpemail(String smtpemail) {
        this.smtpemail = smtpemail;
    }

    public String getSmtppassword() {
        return smtppassword;
    }

    public void setSmtppassword(String smtppassword) {
        this.smtppassword = smtppassword;
    }

    public Boolean getSmtpsecureconnection() {
        return smtpsecureconnection;
    }

    public void setSmtpsecureconnection(Boolean smtpsecureconnection) {
        this.smtpsecureconnection = smtpsecureconnection;
    }

    public Boolean getIsemailsentonorder() {
        return isemailsentonorder;
    }

    public void setIsemailsentonorder(Boolean isemailsentonorder) {
        this.isemailsentonorder = isemailsentonorder;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Object getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(Object countrycode) {
        this.countrycode = countrycode;
    }

    public String getMapapi() {
        return mapapi;
    }

    public void setMapapi(String mapapi) {
        this.mapapi = mapapi;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Boolean getIsMultipleResturentOrderAllow() {
        return isMultipleResturentOrderAllow;
    }

    public void setIsMultipleResturentOrderAllow(Boolean isMultipleResturentOrderAllow) {
        this.isMultipleResturentOrderAllow = isMultipleResturentOrderAllow;
    }

    public Object getIsStripeEnable() {
        return isStripeEnable;
    }

    public void setIsStripeEnable(Object isStripeEnable) {
        this.isStripeEnable = isStripeEnable;
    }

    public Object getIsJudoEnable() {
        return isJudoEnable;
    }

    public void setIsJudoEnable(Object isJudoEnable) {
        this.isJudoEnable = isJudoEnable;
    }

    public Object getIsPaypalEnable() {
        return isPaypalEnable;
    }

    public void setIsPaypalEnable(Object isPaypalEnable) {
        this.isPaypalEnable = isPaypalEnable;
    }

    public Boolean getIsCurrencySymbolEnableInCustomerApp() {
        return isCurrencySymbolEnableInCustomerApp;
    }

    public void setIsCurrencySymbolEnableInCustomerApp(Boolean isCurrencySymbolEnableInCustomerApp) {
        this.isCurrencySymbolEnableInCustomerApp = isCurrencySymbolEnableInCustomerApp;
    }

    public Object getIsCurrencySymbolEnableInDriverApp() {
        return isCurrencySymbolEnableInDriverApp;
    }

    public void setIsCurrencySymbolEnableInDriverApp(Object isCurrencySymbolEnableInDriverApp) {
        this.isCurrencySymbolEnableInDriverApp = isCurrencySymbolEnableInDriverApp;
    }

}
