package com.eurosoft.vendorapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.MasterPojo;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Service.OrderService;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.InputValidatorHelper;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {

    private TextView forgetPass, loginText;
    private Button btnLogin;
    private EditText userNameEt, passwordEt;
    private InputValidatorHelper inputValidatorHelper;
    private MasterPojo masterPojo;
    private ImageView viewPassToggle;
    private RelativeLayout rlProgressBar, mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        inputValidatorHelper = new InputValidatorHelper();
        masterPojo = new MasterPojo();
        Stash.init(this);
        initViews();
    }

    private void initViews() {
        userNameEt = findViewById(R.id.userNameEt);
        passwordEt = findViewById(R.id.passwordEt);
        forgetPass = findViewById(R.id.forgetPass);
        btnLogin = findViewById(R.id.btnLogin);
        viewPassToggle = findViewById(R.id.viewPassToggle);
        loginText = findViewById(R.id.loginText);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);


        progressLoader = new ProgressLoader(ActivityLogin.this,rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityGone();
        loginText.setText(masterPojo.getLogin());
        btnLogin.setText(masterPojo.getLogin());
        userNameEt.setHint(masterPojo.getEmail());
        passwordEt.setHint(masterPojo.getPassword());
        viewPassToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPassToggleState(v);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });


        forgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ActivityForgetPassword.class));
            }
        });
    }

    private void callApiGetAppSettings(String authHeader,LoginResponse loginResponse) {

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<GetAppSettings>> call = apiService.getAppSettings(authHeader);

        call.enqueue(new Callback<WebResponse<GetAppSettings>>() {
            @Override
            public void onResponse(Call<WebResponse<GetAppSettings>> call, Response<WebResponse<GetAppSettings>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (response.code() == 200 && response.body().getSuccess()) {
                    Stash.put(Constants.APP_SETTINGS,response.body().getData());

                    progressLoader.progressVisiblityGone();
                    Stash.put(Constants.LOGIN_OBJ,loginResponse);

                    Stash.put(Constants.IS_LOGGED_IN,true);
                    startService();

                    Intent intent = new Intent(ActivityLogin.this,HomeActivty.class);
                    startActivity(intent);
                    finish();
                    return;
                }


                if (response.code() == 200 && !response.body().getSuccess()) {
                    return;
                }
            }

            @Override
            public void onFailure(Call<WebResponse<GetAppSettings>> call, Throwable t) {

            }
        });
    }

    private void validation() {
        String email = userNameEt.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();


        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toast.makeText(this, masterPojo.getUserNameIsEmpty(), Toast.LENGTH_SHORT).show();
            btnLogin.setClickable(true);

            return;
        }



        if (inputValidatorHelper.isNullOrEmpty(password)) {
            Toast.makeText(this, masterPojo.getPasswordIsEmpty(), Toast.LENGTH_SHORT).show();
            btnLogin.setClickable(true);
            return;
        }

        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(this, masterPojo.getCheckInternet(), Toast.LENGTH_SHORT).show();
            btnLogin.setClickable(true);
            return;
        }

        callApiLogin(email, password);


    }

    private void callApiLogin(String name, String password) {
        progressLoader.progressVisiblityVisible();
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setUserName(name);
        loginResponse.setUserPassword(password);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<LoginResponse>> call = apiService.loginUser(loginResponse);

        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressLoader.progressVisiblityGone();
                    return;
                }
                if (response.code() == 200 && response.body().getSuccess()) {

                    //Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    callApiGetAppSettings(response.body().getData().getToken(),response.body().getData());
                    return;
                }


                if (response.code() == 200 && !response.body().getSuccess()) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
            }
        });
    }


    private void viewPassToggleState(View view) {

        if (passwordEt.getText().toString().trim().equalsIgnoreCase("")) {
            return;
        }

        if (passwordEt.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
            ((ImageView) (view)).setImageResource(R.drawable.ic_eye_closed);
            //Show Password
            passwordEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_remove_red_eye_24);
            //Hide Password
            passwordEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }
    private void startService(){
        Intent serviceIntent = new Intent(ActivityLogin.this, OrderService.class);
        serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
        startService(serviceIntent);
    }
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }
}
