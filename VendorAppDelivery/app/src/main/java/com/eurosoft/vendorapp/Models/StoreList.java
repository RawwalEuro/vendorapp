package com.eurosoft.vendorapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreList implements Parcelable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("StoreTypeId")
    @Expose
    private Integer storeTypeId;
    @SerializedName("StoreTypeName")
    @Expose
    private String storeTypeName;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("OpeningTiming")
    @Expose
    private String openingTiming;
    @SerializedName("ClosingTiming")
    @Expose
    private String closingTiming;
    @SerializedName("StoreDeliveryEstimatedTime")
    @Expose
    private String storeDeliveryEstimatedTime;
    @SerializedName("StoreDeliveryFee")
    @Expose
    private Double storeDeliveryFee;
    @SerializedName("StorePhoneNumber")
    @Expose
    private String storePhoneNumber;
    @SerializedName("StoreBaseUrl")
    @Expose
    private String storeBaseUrl;
    @SerializedName("StoreImage")
    @Expose
    private String storeImage;
    @SerializedName("StoreAddress")
    @Expose
    private String storeAddress;
    @SerializedName("StoreLatitude")
    @Expose
    private Double storeLatitude;
    @SerializedName("StoreLongitude")
    @Expose
    private Double storeLongitude;
    @SerializedName("OrderBy")
    @Expose
    private Integer orderBy;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("PhoneNo")
    @Expose
    private Object phoneNo;
    @SerializedName("IsServiceChargesEnable")
    @Expose
    private Boolean isServiceChargesEnable;
    @SerializedName("ServiceCharges")
    @Expose
    private Object serviceCharges;
    @SerializedName("Tip")
    @Expose
    private Boolean tip;
    @SerializedName("MinimumOrderLimit")
    @Expose
    private Double minimumOrderLimit;
    @SerializedName("StoreLogo")
    @Expose
    private String storeLogo;
    @SerializedName("CreateBy")
    @Expose
    private Integer createBy;
    @SerializedName("IsStoreClosed")
    @Expose
    private Boolean isStoreClosed;
    @SerializedName("TotalRatingCount")
    @Expose
    private Integer totalRatingCount;
    @SerializedName("StoreRating")
    @Expose
    private Double storeRating;

    protected StoreList(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            storeTypeId = null;
        } else {
            storeTypeId = in.readInt();
        }
        storeTypeName = in.readString();
        storeName = in.readString();
        openingTiming = in.readString();
        closingTiming = in.readString();
        storeDeliveryEstimatedTime = in.readString();
        if (in.readByte() == 0) {
            storeDeliveryFee = null;
        } else {
            storeDeliveryFee = in.readDouble();
        }
        storePhoneNumber = in.readString();
        storeBaseUrl = in.readString();
        storeImage = in.readString();
        storeAddress = in.readString();
        if (in.readByte() == 0) {
            storeLatitude = null;
        } else {
            storeLatitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            storeLongitude = null;
        } else {
            storeLongitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            orderBy = null;
        } else {
            orderBy = in.readInt();
        }
        byte tmpIsDefault = in.readByte();
        isDefault = tmpIsDefault == 0 ? null : tmpIsDefault == 1;
        byte tmpStatus = in.readByte();
        status = tmpStatus == 0 ? null : tmpStatus == 1;
        byte tmpIsServiceChargesEnable = in.readByte();
        isServiceChargesEnable = tmpIsServiceChargesEnable == 0 ? null : tmpIsServiceChargesEnable == 1;
        byte tmpTip = in.readByte();
        tip = tmpTip == 0 ? null : tmpTip == 1;
        if (in.readByte() == 0) {
            minimumOrderLimit = null;
        } else {
            minimumOrderLimit = in.readDouble();
        }
        storeLogo = in.readString();
        if (in.readByte() == 0) {
            createBy = null;
        } else {
            createBy = in.readInt();
        }
        byte tmpIsStoreClosed = in.readByte();
        isStoreClosed = tmpIsStoreClosed == 0 ? null : tmpIsStoreClosed == 1;
        if (in.readByte() == 0) {
            totalRatingCount = null;
        } else {
            totalRatingCount = in.readInt();
        }
        if (in.readByte() == 0) {
            storeRating = null;
        } else {
            storeRating = in.readDouble();
        }
    }

    public static final Creator<StoreList> CREATOR = new Creator<StoreList>() {
        @Override
        public StoreList createFromParcel(Parcel in) {
            return new StoreList(in);
        }

        @Override
        public StoreList[] newArray(int size) {
            return new StoreList[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoreTypeId() {
        return storeTypeId;
    }

    public void setStoreTypeId(Integer storeTypeId) {
        this.storeTypeId = storeTypeId;
    }

    public String getStoreTypeName() {
        return storeTypeName;
    }

    public void setStoreTypeName(String storeTypeName) {
        this.storeTypeName = storeTypeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOpeningTiming() {
        return openingTiming;
    }

    public void setOpeningTiming(String openingTiming) {
        this.openingTiming = openingTiming;
    }

    public String getClosingTiming() {
        return closingTiming;
    }

    public void setClosingTiming(String closingTiming) {
        this.closingTiming = closingTiming;
    }

    public String getStoreDeliveryEstimatedTime() {
        return storeDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    public Double getStoreDeliveryFee() {
        return storeDeliveryFee;
    }

    public void setStoreDeliveryFee(Double storeDeliveryFee) {
        this.storeDeliveryFee = storeDeliveryFee;
    }

    public String getStorePhoneNumber() {
        return storePhoneNumber;
    }

    public void setStorePhoneNumber(String storePhoneNumber) {
        this.storePhoneNumber = storePhoneNumber;
    }

    public String getStoreBaseUrl() {
        return storeBaseUrl;
    }

    public void setStoreBaseUrl(String storeBaseUrl) {
        this.storeBaseUrl = storeBaseUrl;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public Double getStoreLatitude() {
        return storeLatitude;
    }

    public void setStoreLatitude(Double storeLatitude) {
        this.storeLatitude = storeLatitude;
    }

    public Double getStoreLongitude() {
        return storeLongitude;
    }

    public void setStoreLongitude(Double storeLongitude) {
        this.storeLongitude = storeLongitude;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Object getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(Object phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Boolean getIsServiceChargesEnable() {
        return isServiceChargesEnable;
    }

    public void setIsServiceChargesEnable(Boolean isServiceChargesEnable) {
        this.isServiceChargesEnable = isServiceChargesEnable;
    }

    public Object getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(Object serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public Boolean getTip() {
        return tip;
    }

    public void setTip(Boolean tip) {
        this.tip = tip;
    }

    public Double getMinimumOrderLimit() {
        return minimumOrderLimit;
    }

    public void setMinimumOrderLimit(Double minimumOrderLimit) {
        this.minimumOrderLimit = minimumOrderLimit;
    }

    public String getStoreLogo() {
        return storeLogo;
    }

    public void setStoreLogo(String storeLogo) {
        this.storeLogo = storeLogo;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Boolean getIsStoreClosed() {
        return isStoreClosed;
    }

    public void setIsStoreClosed(Boolean isStoreClosed) {
        this.isStoreClosed = isStoreClosed;
    }

    public Integer getTotalRatingCount() {
        return totalRatingCount;
    }

    public void setTotalRatingCount(Integer totalRatingCount) {
        this.totalRatingCount = totalRatingCount;
    }

    public Double getStoreRating() {
        return storeRating;
    }

    public void setStoreRating(Double storeRating) {
        this.storeRating = storeRating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (storeTypeId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(storeTypeId);
        }
        dest.writeString(storeTypeName);
        dest.writeString(storeName);
        dest.writeString(openingTiming);
        dest.writeString(closingTiming);
        dest.writeString(storeDeliveryEstimatedTime);
        if (storeDeliveryFee == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(storeDeliveryFee);
        }
        dest.writeString(storePhoneNumber);
        dest.writeString(storeBaseUrl);
        dest.writeString(storeImage);
        dest.writeString(storeAddress);
        if (storeLatitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(storeLatitude);
        }
        if (storeLongitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(storeLongitude);
        }
        if (orderBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderBy);
        }
        dest.writeByte((byte) (isDefault == null ? 0 : isDefault ? 1 : 2));
        dest.writeByte((byte) (status == null ? 0 : status ? 1 : 2));
        dest.writeByte((byte) (isServiceChargesEnable == null ? 0 : isServiceChargesEnable ? 1 : 2));
        dest.writeByte((byte) (tip == null ? 0 : tip ? 1 : 2));
        if (minimumOrderLimit == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(minimumOrderLimit);
        }
        dest.writeString(storeLogo);
        if (createBy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(createBy);
        }
        dest.writeByte((byte) (isStoreClosed == null ? 0 : isStoreClosed ? 1 : 2));
        if (totalRatingCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalRatingCount);
        }
        if (storeRating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(storeRating);
        }
    }
}


