package com.eurosoft.vendorapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterStoreDetailed extends RecyclerView.Adapter<AdapterStoreDetailed.ViewHolder> {

    private AdapterStoreDetailed.OnItemClickListener mOnItemClickListener;
    private List<OrderDataModel.Store> listStore;
    private Context context;

    public AdapterStoreDetailed(Context ctx, List<OrderDataModel.Store> list, AdapterStoreDetailed.OnItemClickListener onItemClickListener) {
        context = ctx;
        listStore = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, StoreList storeList);
    }

    @NonNull
    @Override
    public AdapterStoreDetailed.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_store, parent, false);
        AdapterStoreDetailed.ViewHolder vh = new AdapterStoreDetailed.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterStoreDetailed.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
       OrderDataModel.Store storeList = listStore.get(position);

       holder.name.setText(storeList.getStoreName());
       holder.address.setText(storeList.getStoreAddress());
    }


    @Override
    public int getItemCount() {
        return listStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, address;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.shop_name);
            address = itemView.findViewById(R.id.shop_address);



        }
    }

}

