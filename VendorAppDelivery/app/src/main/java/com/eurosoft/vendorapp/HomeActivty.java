package com.eurosoft.vendorapp;

import static com.eurosoft.vendorapp.Constants.FireBaseOrderParameter;
import static com.eurosoft.vendorapp.Constants.OrderIDJOFFER;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vendorapp.Adapter.AdapterHome;
import com.eurosoft.vendorapp.Fragment.FragmentLeftMenu;
import com.eurosoft.vendorapp.Models.OrderDashBoardStatusPojo;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivty extends AppCompatActivity {

    private RecyclerView rvHome;
    private List<String> strings = new ArrayList<>();
    private AdapterHome adapterHome;
    private DrawerLayout mDrawer;
    private ImageView nav_icon;
    private List<Orders> lstOrder = new ArrayList<>();
    private LoginResponse vendorObj;
    private String status = "Today";
    private ProgressLoader progressLoader;
    private RelativeLayout rlProgressBar, mainRl;
    private CircularProgressBar circularProgressBar;
    private CardView cvCompleted,cvPre,cvPending,cvOnGoing,cvReturn,cvToday;
    private TextView completeCountView,pendingCountView,preCountView,onGoingCountView,todayCountView,returnCountView,todayDateView;
    private Boolean isPendingJob=false;
    private FirebaseDatabase mFirebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
      /*  if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }*/
        Stash.init(this);
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        initViews();
    }

    private void callApiHome(String status) {
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<OrderDashBoardStatusPojo>> call = apiService.dashboardAll(vendorObj.getId()+"",status,vendorObj.getToken()+"");

        call.enqueue(new Callback<WebResponse<OrderDashBoardStatusPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<OrderDashBoardStatusPojo>> call, Response<WebResponse<OrderDashBoardStatusPojo>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressLoader.progressVisiblityGone();
                    return;
                }


                if (response.code() == 200 && response.body().getSuccess()) {
                    progressLoader.progressVisiblityGone();
                    preCountView.setText(""+response.body().getData().getOrderCount().getTotalPreOrders());
                    completeCountView.setText(""+response.body().getData().getOrderCount().getTotalCompletedOrders());
                    pendingCountView.setText(""+response.body().getData().getOrderCount().getPendingOrders());
                    onGoingCountView.setText(""+response.body().getData().getOrderCount().getTotalRecentOrders());
                    returnCountView.setText(""+response.body().getData().getOrderCount().getTotalReturnOrders());
                    todayCountView.setText(""+response.body().getData().getOrderCount().getTotalTodayOrders());
                    lstOrder = response.body().getData().getOrderList();
                    if(lstOrder!=null) {
                        adapterHome = new AdapterHome(HomeActivty.this, lstOrder, new AdapterHome.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position,Orders orders) {
                                switch (view.getId()){
                                    case R.id.cardView:
                                        if(isPendingJob) {
                                            CustomDialogClass customDialogClass = new CustomDialogClass(HomeActivty.this);
                                            customDialogClass.show();
                                            customDialogClass.setCancelable(false);
                                        }
                                        else {
                                            Intent intent=new Intent(HomeActivty.this,ActivityOrderDetailed.class);
                                            intent.putExtra("orderId",orders.getId()+"");
                                            startActivity(intent);
                                        }
                                        break;
                                }

                            }
                        });

                        rvHome.setAdapter(adapterHome);
                        adapterHome.notifyDataSetChanged();
                    }
                    else {
                        filter("null");
                        if(status.equals("Pen")){
                            Toast.makeText(getApplicationContext(), "Pending Orders Not Found", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Toast.makeText(getApplicationContext(), ""+status+" Orders Not Found", Toast.LENGTH_SHORT).show();

                        }
                    }
                    return;
                }

                if (response.code() == 200 && !response.body().getSuccess()) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    return;
                }
            }

            @Override
            public void onFailure(Call<WebResponse<OrderDashBoardStatusPojo>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
            }
        });
    }

    private void initViews() {



        rvHome = findViewById(R.id.rvHome);
        mDrawer = findViewById(R.id.drawer_layout);
        nav_icon = findViewById(R.id.nav_icon);
        cvCompleted = findViewById(R.id.cvCompleted);
        cvPre = findViewById(R.id.cvPre);
        cvReturn = findViewById(R.id.cvReturn);
        cvToday = findViewById(R.id.cvToday);
        cvPending = findViewById(R.id.cvPending);
        cvOnGoing = findViewById(R.id.cvOnGoing);
        completeCountView=findViewById(R.id.completeCount);
        pendingCountView=findViewById(R.id.pendingCount);
        preCountView=findViewById(R.id.PreCount);
        onGoingCountView=findViewById(R.id.onGoingCount);
        todayCountView=findViewById(R.id.todayCount);
        returnCountView=findViewById(R.id.onReturnCount);
        todayDateView=findViewById(R.id.todayDate);
        todayDateView.setText(getCurrentDate("dd MMM,yyyy"));
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        cvCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getApplicationContext(),"Completed",Toast.LENGTH_SHORT).show();
                status = "Completed";
                isPendingJob=false;
                callApiHome(status);
            }
        });


        cvPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(getApplicationContext(),"Pre",Toast.LENGTH_SHORT).show();
                status = "Pre";
                isPendingJob=false;
                callApiHome(status);
            }
        });

        cvToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(getApplicationContext(),"Today",Toast.LENGTH_SHORT).show();
                status = "Today";
                isPendingJob=false;
                callApiHome(status);
            }
        });



        cvReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    Toast.makeText(getApplicationContext(),"Return",Toast.LENGTH_SHORT).show();
                status = "Return";
                isPendingJob=false;
                callApiHome(status);
            }
        });



        cvPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Toast.makeText(getApplicationContext(),"Pending",Toast.LENGTH_SHORT).show();
                isPendingJob=true;
                status = "Pen";
                callApiHome(status);
            }
        });


          cvOnGoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //    Toast.makeText(getApplicationContext(),"OnGoing",Toast.LENGTH_SHORT).show();
                status = "Recent";
                isPendingJob=false;
                callApiHome(status);
            }
        });
          findViewById(R.id.filter_icon).setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  startActivity(new Intent(HomeActivty.this, ActivityFilter.class));
              }
          });
        findViewById(R.id.bell_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivty.this, ActivityStoreList.class));
            }
        });


        rvHome.setLayoutManager(new LinearLayoutManager(HomeActivty.this,LinearLayoutManager.VERTICAL,false));
        rvHome.setHasFixedSize(true);



        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);


        progressLoader = new ProgressLoader(HomeActivty.this,rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityGone();



        mDrawer.setScrimColor(getResources().getColor(R.color.semi_grey));
        getSupportFragmentManager().beginTransaction().replace(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();
        mDrawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START, true);
            }
        });

        callApiHome(status);
    }

    public String getCurrentDate(String dateformat) {
        Date Date = new Date();
        String TodayDate = String.valueOf(DateFormat.format(dateformat, Date.getTime()));
        return TodayDate;
    }
    private void filter(String text) {
        ArrayList<Orders> filteredList = new ArrayList<>();
        if (lstOrder != null) {
            for (Orders item : lstOrder) {
                if (item.getOrderStatus().toLowerCase().startsWith(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        if (adapterHome != null) {
            adapterHome.filterList(filteredList);
        }
    }
    public class CustomDialogClass extends Dialog implements android.view.View.OnClickListener {

        public Activity activity;
        public Dialog dialog;
        public LinearLayout yes, no;

        public CustomDialogClass(Activity activity) {
            super(activity);
            // TODO Auto-generated constructor stub
            this.activity = activity;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            dialog=new Dialog(HomeActivty.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            setContentView(R.layout.custom_dialog1);
            yes =  findViewById(R.id.accept_order);
            no =  findViewById(R.id.reject_order);
            yes.setOnClickListener(this);
            no.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.accept_order:
                    callApiForOrderStatus(Constants.Accepted,OrderIDJOFFER);
                    dismiss();
                    isPendingJob=false;
                    break;
                case R.id.reject_order:
                    callApiForOrderStatus(Constants.Cancelled,OrderIDJOFFER);
                    isPendingJob=false;
                    dismiss();
                    break;
            }
        }
    }
    private void callApiForOrderStatus(int statusId,int orderId){
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;
        }
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse> call = apiService.getOrderStatus(orderId+"",vendorObj.getId().toString(),statusId,vendorObj.getToken()+"");
        call.enqueue(new Callback<WebResponse>() {
            @Override
            public void onResponse(Call<WebResponse> call, Response<WebResponse> response) {

                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();

                    if (sucess) {
                        progressLoader.progressVisiblityGone();
                        deleteDataFromFireBase();
                        OrderIDJOFFER=0;
                    } else {
                        Toast.makeText(HomeActivty.this, "" + message, Toast.LENGTH_LONG).show();
                        progressLoader.progressVisiblityGone();

                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(HomeActivty.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponse> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(HomeActivty.this, "" + t.getMessage(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }
    public void deleteDataFromFireBase() {
        Query applesQuery = mFirebaseDatabase.getReference().child("1").child(vendorObj.getId().toString());
        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot mdataSnapshot : dataSnapshot.getChildren()) {
                    mdataSnapshot.getRef().child(FireBaseOrderParameter).removeValue();
                }
                OrderIDJOFFER=0;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}