package com.eurosoft.vendorapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.eurosoft.vendorapp.Adapter.AdapterHome;
import com.eurosoft.vendorapp.Models.MasterPojo;
import com.eurosoft.vendorapp.Models.OrderDashBoardStatusPojo;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Utils.AppStatus;
import com.eurosoft.vendorapp.Utils.InputValidatorHelper;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityForgetPassword extends AppCompatActivity {
    private TextView forgetPass;
    private EditText userNameEt;
    private Button btnResetPass;
    private MasterPojo masterPojo;
    private InputValidatorHelper inputValidatorHelper;
    private ProgressLoader progressLoader;
    private RelativeLayout rlProgressBar, mainRl;
    private CircularProgressBar circularProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        masterPojo = new MasterPojo();
        inputValidatorHelper = new InputValidatorHelper();
        initViews();

    }

    private void initViews() {
        forgetPass = findViewById(R.id.forgetPass);
        userNameEt = findViewById(R.id.emaiilEt);
        btnResetPass = findViewById(R.id.btnResetPass);

        userNameEt.setHint(masterPojo.getEmail());
        forgetPass.setText(masterPojo.getForgotPass());
        btnResetPass.setText(masterPojo.getResetPassword());

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);

        progressLoader = new ProgressLoader(ActivityForgetPassword.this,rlProgressBar, circularProgressBar, mainRl);
        progressLoader.progressVisiblityGone();

        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(userNameEt.getText().toString().trim());
            }
        });
    }

    private void validate(String email) {
        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toast.makeText(this, masterPojo.getUserNameIsEmpty(), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!inputValidatorHelper.isValidEmail(email)) {
            Toast.makeText(this, masterPojo.getNotAValidEmail(), Toast.LENGTH_SHORT).show();
            return;
        }

        callApiForget(email);
    }

    private void callApiForget(String email) {
        progressLoader.progressVisiblityVisible();
        if (!AppStatus.getInstance(this).isOnline()) {
            progressLoader.progressVisiblityGone();
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<String>> call = apiService.forgetPassWord(email);

        call.enqueue(new Callback<WebResponse<String>>() {


            @Override
            public void onResponse(Call<WebResponse<String>> call, Response<WebResponse<String>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (response.code() == 200 && response.body().getSuccess()) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }

                if (response.code() == 200 && !response.body().getSuccess()) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            @Override
            public void onFailure(Call<WebResponse<String>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
            }
        });

    }


}
