package com.eurosoft.vendorapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse {
    @SerializedName("fromDate")
    @Expose
    private String fromDate;
    @SerializedName("toDate")
    @Expose
    private String toDate;
    @SerializedName("customerId")
    @Expose
    private Object customerId;
    @SerializedName("paymentMethodId")
    @Expose
    private Object paymentMethodId;
    @SerializedName("shippingAddress")
    @Expose
    private Object shippingAddress;
    @SerializedName("driverId")
    @Expose
    private Object driverId;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("bookingStatusId")
    @Expose
    private Object bookingStatusId;
    @SerializedName("orderNumber")
    @Expose
    private Object orderNumber;
    @SerializedName("deviceId")
    @Expose
    private Object deviceId;
    @SerializedName("userId")
    @Expose
    private Object userId;

    public SearchResponse(String fromDate, String toDate, Object customerId, Object paymentMethodId, Object shippingAddress, Object driverId, Object phoneNumber, Object bookingStatusId, Object orderNumber, Object deviceId, Integer userId) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.customerId = customerId;
        this.paymentMethodId = paymentMethodId;
        this.shippingAddress = shippingAddress;
        this.driverId = driverId;
        this.phoneNumber = phoneNumber;
        this.bookingStatusId = bookingStatusId;
        this.orderNumber = orderNumber;
        this.deviceId = deviceId;
        this.userId = userId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }


    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }


    public void setPaymentMethodId(Integer paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Object getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Object shippingAddress) {
        this.shippingAddress = shippingAddress;
    }


    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public void setBookingStatusId(Integer bookingStatusId) {
        this.bookingStatusId = bookingStatusId;
    }

    public Object getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Object orderNumber) {
        this.orderNumber = orderNumber;
    }


    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }


    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
