package com.eurosoft.vendorapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.Store;
import com.eurosoft.vendorapp.R;

import java.util.List;

public class AdapterMenu extends RecyclerView.Adapter<AdapterMenu.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private List<GetAppSettings.Menu> listStore;
    private Context context;

    public AdapterMenu(Context ctx, List<GetAppSettings.Menu> list, OnItemClickListener onItemClickListener) {
        context = ctx;
        listStore = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position,String key);
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_menu, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        GetAppSettings.Menu menu = listStore.get(position);
        holder.menuName.setText("" + menu.getMenuName());

        holder.menusRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v,position,holder.menuName.getText().toString());
            }
        });


    }


    @Override
    public int getItemCount() {
        return listStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView menuName;
        RelativeLayout menusRl;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            menuName = itemView.findViewById(R.id.menu);
            menusRl=itemView.findViewById(R.id.menusRl);

        }
    }
}
