package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDashBoardStatusPojo {

    @SerializedName("OrderCount")
    @Expose
    private OrderCount orderCount;
    @SerializedName("OrderList")
    @Expose
    private List<Orders> orderList;

    public OrderCount getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(OrderCount orderCount) {
        this.orderCount = orderCount;
    }

    public List<Orders> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Orders> orderList) {
        this.orderList = orderList;
    }
}
