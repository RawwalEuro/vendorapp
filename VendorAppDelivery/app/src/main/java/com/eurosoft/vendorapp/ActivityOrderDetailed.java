package com.eurosoft.vendorapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vendorapp.Adapter.AdapterHome;
import com.eurosoft.vendorapp.Adapter.AdapterProduct;
import com.eurosoft.vendorapp.Adapter.AdapterStoreDetailed;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.MasterPojo;
import com.eurosoft.vendorapp.Models.OrderDataModel;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.Network.APIClient;
import com.eurosoft.vendorapp.Network.ApiInterface;
import com.eurosoft.vendorapp.Pojo.LoginResponse;
import com.eurosoft.vendorapp.Utils.ProgressLoader;
import com.eurosoft.vendorapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityOrderDetailed extends AppCompatActivity {

    private MasterPojo masterPojo;
    private LoginResponse vendorObj;
    private ApiInterface apiInterface;
    private RelativeLayout rlProgressBar, mainRl;
    private ProgressLoader progressLoader;
    private CircularProgressBar circularProgressBar;
    private TextView orderIdTv, customer_details, product_detail_title, customer_name, customer_address, customer_no, delivery_time, card_no, sub_total, subtotal_amount, delivery_fees_title, delivery_fees, counpon_title, counpon_disaccount, sc_title, services_charger, total_payment, totaltaxheading
            ,special_instruction;
    private LinearLayout parentLl,specail_instructuon_layout,driver_instruction_layout;
    private RelativeLayout servicesChargesRR,counponRR;
    private RecyclerView storelist_recycleview, productlist_recycleview;
    private AdapterProduct adapterProduct;
    private List<OrderDataModel.Product> product=new ArrayList<>();
    private AdapterStoreDetailed adapterStoreDetailed;
    private GetAppSettings appSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detailed);
        initView();
    }

    public void getOrderData(String userid, String orderId) {
        Call<WebResponse<OrderDataModel>> call = apiInterface.getOrderData(userid, orderId, vendorObj.getToken() + "");
        call.enqueue(new Callback<WebResponse<OrderDataModel>>() {
            @Override
            public void onResponse(Call<WebResponse<OrderDataModel>> call, Response<WebResponse<OrderDataModel>> response) {
                try {
                    WebResponse webResponse = response.body();
                    Boolean sucess = webResponse.getSuccess();
                    String message = webResponse.getMessage();
                    OrderDataModel orderDataModel = (OrderDataModel) webResponse.getData();
                    if (sucess) {
                        progressLoader.progressVisiblityGone();
                        setMethod(orderDataModel);
                    } else {
                        progressLoader.progressVisiblityGone();
                        Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    progressLoader.progressVisiblityGone();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.e("ExceptionTag", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<OrderDataModel>> call, Throwable t) {
                progressLoader.progressVisiblityGone();
                Toast.makeText(getApplicationContext(), masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                call.cancel();
            }
        });
    }

    public void initView() {
        masterPojo = new MasterPojo();
        vendorObj = (LoginResponse) Stash.getObject(Constants.LOGIN_OBJ, LoginResponse.class);
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        progressLoader = new ProgressLoader(ActivityOrderDetailed.this, rlProgressBar, circularProgressBar, mainRl);
        orderIdTv = findViewById(R.id.orderid);
        parentLl = findViewById(R.id.parentLl);
        storelist_recycleview = findViewById(R.id.storelist_recycleview);
        productlist_recycleview = findViewById(R.id.productlist_recycleview);
        customer_details = findViewById(R.id.customer_details);
        product_detail_title = findViewById(R.id.product_detail_title);
        customer_name = findViewById(R.id.customer_name);
        customer_address = findViewById(R.id.customer_address);
        customer_no = findViewById(R.id.customer_no);
        delivery_time = findViewById(R.id.delivery_time);
        card_no = findViewById(R.id.card_no);
        sub_total = findViewById(R.id.sub_total);
        subtotal_amount = findViewById(R.id.subtotal_amount);
        delivery_fees_title = findViewById(R.id.delivery_fees_title);
        delivery_fees = findViewById(R.id.delivery_fees);
        counpon_title = findViewById(R.id.counpon_title);
        counpon_disaccount = findViewById(R.id.counpon_disaccount);
        sc_title = findViewById(R.id.sc_title);
        services_charger = findViewById(R.id.services_charger);
        total_payment = findViewById(R.id.total_payment);
        totaltaxheading = findViewById(R.id.totaltaxheading);
        specail_instructuon_layout=findViewById(R.id.specail_instructuon_layout);
        driver_instruction_layout=findViewById(R.id.driver_instruction_layout);
        special_instruction=findViewById(R.id.special_instruction);
        servicesChargesRR=findViewById(R.id.services_charges_RR);
        counponRR=findViewById(R.id.counpon_RR);
        appSettings = (GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);


        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("orderId") != null) {
            getOrderData(vendorObj.getId() + "", bundle.getString("orderId"));
        }

        findViewById(R.id.nav_icon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setMethod(OrderDataModel orderDataModel) {
        DecimalFormat df = new DecimalFormat("0.00");
        parentLl.setVisibility(View.VISIBLE);
        orderIdTv.setText("Order ID: #" + orderDataModel.getOrderNo());
        customer_name.setText(orderDataModel.getCustomerName());
        customer_address.setText(orderDataModel.getDropoffAddress());
        customer_no.setText(orderDataModel.getCustomerNo());
        delivery_time.setText(orderDataModel.getDeliveryTime());
        card_no.setText(orderDataModel.getPaymentTypeName());

        subtotal_amount.setText(appSettings.getRegionSettingData().getCurrencySymbol() + df.format(+orderDataModel.getSubTotal()));
        if (orderDataModel.getShippingCharges() == null) {
            delivery_fees.setText("--");
        } else {
            delivery_fees.setText(appSettings.getRegionSettingData().getCurrencySymbol() + df.format(orderDataModel.getShippingCharges()));
        }
        total_payment.setText(appSettings.getRegionSettingData().getCurrencySymbol() + orderDataModel.getTotalAmount());

        if(orderDataModel.getInstruction()==null){
            driver_instruction_layout.setVisibility(View.GONE);
            specail_instructuon_layout.setVisibility(View.GONE);
        }
        else {
            driver_instruction_layout.setVisibility(View.VISIBLE);
            specail_instructuon_layout.setVisibility(View.VISIBLE);
            special_instruction.setText(orderDataModel.getInstruction() == null ? "" : orderDataModel.getInstruction());
        }
        if (orderDataModel.getTipCharges()!=null && orderDataModel.getTipCharges()!=0.0) {
           counponRR.setVisibility(View.VISIBLE);
            counpon_disaccount.setText(appSettings.getRegionSettingData().getCurrencySymbol() +orderDataModel.getTipCharges());
        }

        if (orderDataModel.getServicesCharges()!=null && orderDataModel.getServicesCharges()!=0.0) {
          servicesChargesRR.setVisibility(View.VISIBLE);
          services_charger.setText(appSettings.getRegionSettingData().getCurrencySymbol() +orderDataModel.getServicesCharges());
        }
        setupRecycleView(orderDataModel);
    }
    public void setupRecycleView(OrderDataModel orderDataModel) {
            adapterStoreDetailed = new AdapterStoreDetailed(ActivityOrderDetailed.this, orderDataModel.getStoreList(), new AdapterStoreDetailed.OnItemClickListener() {
                @Override
                public void onItemClick(View view, StoreList storeList) {

                }
            });
            for(int i=0; i<orderDataModel.getStoreList().size(); i++){
                if (orderDataModel.getStoreList().get(i).getProductList() != null) {
                    for (int j = 0; j <orderDataModel.getStoreList().get(i).getProductList().size(); j++) {
                        Log.e("OrderActivity", "Product-List " + orderDataModel.getStoreList().get(i).getProductList().get(j).getProductName());
                        product.add(orderDataModel.getStoreList().get(i).getProductList().get(j));
                    }
                    if (product.size() != 0) {
                        adapterProduct = new AdapterProduct(ActivityOrderDetailed.this, product, new AdapterProduct.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, StoreList storeList) {
                            }
                        });
                    }
                }
            }


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(ActivityOrderDetailed.this,
                DividerItemDecoration.VERTICAL);
        storelist_recycleview.addItemDecoration(dividerItemDecoration);
       productlist_recycleview.addItemDecoration(dividerItemDecoration);
        storelist_recycleview.setLayoutManager(new LinearLayoutManager(ActivityOrderDetailed.this));
        productlist_recycleview.setLayoutManager(new LinearLayoutManager(ActivityOrderDetailed.this));
        storelist_recycleview.setAdapter(adapterStoreDetailed);
        productlist_recycleview.setAdapter(adapterProduct);
    }

}