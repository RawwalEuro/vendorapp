package com.eurosoft.vendorapp.Adapter;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.Store;
import com.eurosoft.vendorapp.R;

import java.util.Calendar;
import java.util.List;

public class AdapterStoreTiming extends RecyclerView.Adapter<AdapterStoreTiming.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private List<Store.StoreTimingList> listStore;
    private Context context;
    private Calendar calendar;

    public AdapterStoreTiming(Context ctx, List<Store.StoreTimingList> list, OnItemClickListener onItemClickListener) {
        context = ctx;
        listStore = list;
        mOnItemClickListener = onItemClickListener;
    }


    public List<Store.StoreTimingList> getListStore() {
        return listStore;
    }

    public void setListStore(List<Store.StoreTimingList> listStore) {
        this.listStore = listStore;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        calendar = Calendar.getInstance();
        View v = inflater.inflate(R.layout.item_storelist, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        Store.StoreTimingList storeTimingList = listStore.get(position);
        holder.store_statusCB.setChecked(storeTimingList.getIsStoreClose());
        holder.current_day.setText("" + storeTimingList.getDay());
        holder.startDateEt.setText("" + storeTimingList.getOpeningTiming());
        holder.endDateEt.setText("" + storeTimingList.getClosingTiming());
        holder.close_reasonET.setText("" + storeTimingList.getStoreCloseReason());


        holder.startDateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holder.startDateEt.setText(selectedHour + ":" + selectedMinute);
                        storeTimingList.setOpeningTiming(selectedHour + ":" + selectedMinute);
                        listStore.set(holder.getAdapterPosition(),storeTimingList);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });


        holder.endDateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holder.endDateEt.setText(selectedHour + ":" + selectedMinute);
                        storeTimingList.setClosingTiming(selectedHour + ":" + selectedMinute);
                        listStore.set(holder.getAdapterPosition(),storeTimingList);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
    }





    @Override
    public int getItemCount() {
        return listStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        EditText startDateEt, endDateEt, close_reasonET;
        TextView store_title, tip_enable_title, openingTimeTitle, closingTimeTitle, closeReasonTitle, current_day;
        CheckBox store_statusCB;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            store_title = itemView.findViewById(R.id.store_title);
            current_day = itemView.findViewById(R.id.current_day);
            tip_enable_title = itemView.findViewById(R.id.tip_enable_title);
            openingTimeTitle = itemView.findViewById(R.id.openingTimeTitle);
            closingTimeTitle = itemView.findViewById(R.id.closingTimeTitle);
            closeReasonTitle = itemView.findViewById(R.id.closeReasonTitle);
            store_statusCB = itemView.findViewById(R.id.store_statusCB);
            startDateEt = itemView.findViewById(R.id.startDateEt);
            endDateEt = itemView.findViewById(R.id.endDateEt);
            close_reasonET = itemView.findViewById(R.id.close_reasonET);

        }
    }

}


