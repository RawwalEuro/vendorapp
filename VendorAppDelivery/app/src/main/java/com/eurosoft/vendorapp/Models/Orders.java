package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Orders implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("StoreAddress")
    @Expose
    private String storeAddress;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("OrderTime")
    @Expose
    private String orderTime;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("CustomerFirstName")
    @Expose
    private String customerFirstName;
    @SerializedName("CustomerLastName")
    @Expose
    private String customerLastName;
    @SerializedName("CustomerPhoneNumber")
    @Expose
    private String customerPhoneNumber;
    @SerializedName("DropoffAddress")
    @Expose
    private String dropoffAddress;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("CouponDiscount")
    @Expose
    private Double couponDiscount;
    @SerializedName("GrandTotal")
    @Expose
    private Object grandTotal;
    @SerializedName("OrderStatusId")
    @Expose
    private Integer orderStatusId;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("PaymentTypeName")
    @Expose
    private String paymentTypeName;
    @SerializedName("CallRefNo")
    @Expose
    private Object callRefNo;
    @SerializedName("BookingTypeId")
    @Expose
    private Integer bookingTypeId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ZoneName")
    @Expose
    private String zoneName;
    @SerializedName("ZoneId")
    @Expose
    private Object zoneId;
    @SerializedName("DriverId")
    @Expose
    private Object driverId;
    @SerializedName("DriverNo")
    @Expose
    private String driverNo;
    @SerializedName("Instruction")
    @Expose
    private String instruction;
    @SerializedName("IsBulkOrder")
    @Expose
    private Object isBulkOrder;
    @SerializedName("PaymentStatusId")
    @Expose
    private Integer paymentStatusId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(Double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Object getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Object grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public Object getCallRefNo() {
        return callRefNo;
    }

    public void setCallRefNo(Object callRefNo) {
        this.callRefNo = callRefNo;
    }

    public Integer getBookingTypeId() {
        return bookingTypeId;
    }

    public void setBookingTypeId(Integer bookingTypeId) {
        this.bookingTypeId = bookingTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Object getZoneId() {
        return zoneId;
    }

    public void setZoneId(Object zoneId) {
        this.zoneId = zoneId;
    }

    public Object getDriverId() {
        return driverId;
    }

    public void setDriverId(Object driverId) {
        this.driverId = driverId;
    }

    public String getDriverNo() {
        return driverNo;
    }

    public void setDriverNo(String driverNo) {
        this.driverNo = driverNo;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Object getIsBulkOrder() {
        return isBulkOrder;
    }

    public void setIsBulkOrder(Object isBulkOrder) {
        this.isBulkOrder = isBulkOrder;
    }

    public Integer getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(Integer paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }
}
