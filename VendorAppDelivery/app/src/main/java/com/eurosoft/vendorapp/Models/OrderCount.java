package com.eurosoft.vendorapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderCount {
    @SerializedName("TotalTodayOrders")
    @Expose
    private Integer totalTodayOrders;
    @SerializedName("TotalPreOrders")
    @Expose
    private Integer totalPreOrders;
    @SerializedName("TotalRecentOrders")
    @Expose
    private Integer totalRecentOrders;
    @SerializedName("TotalReturnOrders")
    @Expose
    private Integer totalReturnOrders;
    @SerializedName("TotalCompletedOrders")
    @Expose
    private Integer totalCompletedOrders;
    @SerializedName("TotalCancelledOrder")
    @Expose
    private Integer totalCancelledOrder;
    @SerializedName("TotalBulkOrders")
    @Expose
    private Integer totalBulkOrders;
    @SerializedName("PendingOrders")
    @Expose
    private Integer pendingOrders;
    @SerializedName("StorePendingRequest")
    @Expose
    private Integer storePendingRequest;

    public Integer getTotalTodayOrders() {
        return totalTodayOrders;
    }

    public void setTotalTodayOrders(Integer totalTodayOrders) {
        this.totalTodayOrders = totalTodayOrders;
    }

    public Integer getTotalPreOrders() {
        return totalPreOrders;
    }

    public void setTotalPreOrders(Integer totalPreOrders) {
        this.totalPreOrders = totalPreOrders;
    }

    public Integer getTotalRecentOrders() {
        return totalRecentOrders;
    }

    public void setTotalRecentOrders(Integer totalRecentOrders) {
        this.totalRecentOrders = totalRecentOrders;
    }

    public Integer getTotalReturnOrders() {
        return totalReturnOrders;
    }

    public void setTotalReturnOrders(Integer totalReturnOrders) {
        this.totalReturnOrders = totalReturnOrders;
    }

    public Integer getTotalCompletedOrders() {
        return totalCompletedOrders;
    }

    public void setTotalCompletedOrders(Integer totalCompletedOrders) {
        this.totalCompletedOrders = totalCompletedOrders;
    }

    public Integer getTotalCancelledOrder() {
        return totalCancelledOrder;
    }

    public void setTotalCancelledOrder(Integer totalCancelledOrder) {
        this.totalCancelledOrder = totalCancelledOrder;
    }

    public Integer getTotalBulkOrders() {
        return totalBulkOrders;
    }

    public void setTotalBulkOrders(Integer totalBulkOrders) {
        this.totalBulkOrders = totalBulkOrders;
    }

    public Integer getPendingOrders() {
        return pendingOrders;
    }

    public void setPendingOrders(Integer pendingOrders) {
        this.pendingOrders = pendingOrders;
    }

    public Integer getStorePendingRequest() {
        return storePendingRequest;
    }

    public void setStorePendingRequest(Integer storePendingRequest) {
        this.storePendingRequest = storePendingRequest;
    }
}
