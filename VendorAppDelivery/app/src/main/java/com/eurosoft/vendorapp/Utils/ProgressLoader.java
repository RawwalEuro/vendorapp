package com.eurosoft.vendorapp.Utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

public class ProgressLoader {

    private RelativeLayout progressLayout,mainLayout;
    private Context context;
    private TextView msgText;
    private CircularProgressBar circularProgressBar;


    public ProgressLoader(Context ctx, RelativeLayout rlProgressbar, CircularProgressBar circularProgressBars, RelativeLayout mainRl){
        this.context = ctx;
        this.progressLayout= rlProgressbar;
        this.mainLayout= mainRl;
        this.circularProgressBar= circularProgressBars;
    }


    public void progressVisiblityVisible(){
        progressLayout.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }


    public void progressVisiblityGone(){

        progressLayout.setVisibility(View.GONE);
        circularProgressBar.setIndeterminateMode(false);
    }
}
