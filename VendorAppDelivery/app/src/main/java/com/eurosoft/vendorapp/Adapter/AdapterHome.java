package com.eurosoft.vendorapp.Adapter;

import static com.eurosoft.vendorapp.Constants.AcceptPending;
import static com.eurosoft.vendorapp.Constants.Collect;
import static com.eurosoft.vendorapp.Constants.arrival;
import static com.eurosoft.vendorapp.Constants.completed_order;
import static com.eurosoft.vendorapp.Constants.decline_order;
import static com.eurosoft.vendorapp.Constants.deliver;
import static com.eurosoft.vendorapp.Constants.go_to_customer;
import static com.eurosoft.vendorapp.Constants.waiting_order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Constants;
import com.eurosoft.vendorapp.Models.GetAppSettings;
import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.R;
import com.fxn.stash.Stash;

import java.nio.channels.AcceptPendingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.ViewHolder> {


    private OnItemClickListener mOnItemClickListener;
    private List<Orders> listOrders;
    private Context context;
    DecimalFormat df;
    private GetAppSettings appSettings;

    public AdapterHome(Context ctx, List<Orders> list, AdapterHome.OnItemClickListener onItemClickListener) {
        context = ctx;
        listOrders = list;
        mOnItemClickListener = onItemClickListener;
        df = new DecimalFormat("0.00");
        appSettings = (GetAppSettings) Stash.getObject(Constants.APP_SETTINGS, GetAppSettings.class);
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position,Orders orders);
    }

    @NonNull
    @Override
    public AdapterHome.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_card_home, parent, false);
        AdapterHome.ViewHolder vh = new AdapterHome.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHome.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Orders orders = listOrders.get(position);
        holder.customerName.setText(orders.getCustomerFirstName() + " " + orders.getCustomerLastName());
        holder.orderId.setText(orders.getOrderNo());
        holder.status.setText(orders.getOrderStatus());
        setOrderStatus(holder.statusRR,orders.getOrderStatus());
        holder.paymentType.setText(orders.getPaymentTypeName());
        holder.totalValue.setText(appSettings.getRegionSettingData().getCurrencySymbol()+df.format(orders.getTotalAmount()) + "");
        holder.dateTime.setText(changeDateFormat(orders.getOrderDate()));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v,position,orders);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listOrders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView orderId,dateTime,status,paymentType,customerName,totalValue;
        private CardView cardView;
        private RelativeLayout statusRR;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            orderId = itemView.findViewById(R.id.orderId);
            dateTime = itemView.findViewById(R.id.dateTime);
            status = itemView.findViewById(R.id.status);
            customerName = itemView.findViewById(R.id.customerName);
            paymentType = itemView.findViewById(R.id.paymentType);
            totalValue = itemView.findViewById(R.id.totalValue);
            cardView=itemView.findViewById(R.id.cardView);
            statusRR=itemView.findViewById(R.id.statusRR);
        }
    }
    public void filterList(ArrayList<Orders> filteredList) {
        listOrders = filteredList;
        notifyDataSetChanged();
    }
    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = "dd MMM yyyy HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    private void setOrderStatus(RelativeLayout statusRR, String status){
        if(status.equals(AcceptPending)){
            statusRR.getBackground().setTint(Color.parseColor("#FF69B4"));
        }
        else if(status.equals(Collect)){
            statusRR.getBackground().setTint(Color.parseColor("#8A2BE2"));

        }
        else if(status.equals(go_to_customer)){
            statusRR.getBackground().setTint(Color.parseColor("#FF8C00"));

        }
        else if(status.equals(arrival)){
            statusRR.getBackground().setTint(Color.parseColor("#000000"));

        }
        else if(status.equals(completed_order) || status.equals(deliver)){
            statusRR.getBackground().setTint(Color.parseColor("#108510"));
        }
        else if(status.equals(waiting_order)){

            statusRR.getBackground().setTint(Color.parseColor("#4169E1"));
        }
        else if(status.equals(decline_order)){
            statusRR.getBackground().setTint(Color.parseColor("#EC2E15"));
        }

    }
}
