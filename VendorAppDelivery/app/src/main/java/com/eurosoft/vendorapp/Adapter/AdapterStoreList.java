package com.eurosoft.vendorapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vendorapp.Models.Orders;
import com.eurosoft.vendorapp.Models.StoreList;
import com.eurosoft.vendorapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterStoreList extends RecyclerView.Adapter<AdapterStoreList.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private ArrayList<StoreList> listStore;
    private Context context;

    public AdapterStoreList(Context ctx, ArrayList<StoreList> list, OnItemClickListener onItemClickListener) {
        context = ctx;
        listStore = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, StoreList storeList);
    }

    @NonNull
    @Override
    public AdapterStoreList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_store_list, parent, false);
        AdapterStoreList.ViewHolder vh = new AdapterStoreList.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterStoreList.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        StoreList storeList=listStore.get(position);
        holder.storeNameTv.setText(""+storeList.getStoreName());
        holder.storeTypeTv.setText(""+storeList.getStoreTypeName());
        holder.storeTypeId.setText(""+storeList.getId());
        if(storeList.getStatus()){
            holder.storeStatus.setText("Active");
        }
        else {
            holder.storeStatus.setText("Deactive");
        }

        holder.editStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v,storeList);
            }
        });




    }


    @Override
    public int getItemCount() {
        return listStore.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView editStore;
        TextView storeTypeTv, storeNameTv, storeTypeId, storeStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            storeTypeTv = itemView.findViewById(R.id.storeTypeTv);
            storeNameTv = itemView.findViewById(R.id.storeNameTv);
            storeTypeId = itemView.findViewById(R.id.storeTypeId);
            storeStatus = itemView.findViewById(R.id.storeStatus);
            editStore = itemView.findViewById(R.id.editStore);


        }
    }
    public void filterList(ArrayList<StoreList> filteredList) {
        listStore = filteredList;
        notifyDataSetChanged();
    }


}


